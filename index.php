<?php
require_once 'include/config.inc.php';
class IndexTest extends CLPublicPage {
    public function dispatch() {
        $ctx = array();
        return $this->render('index.html.twig', $ctx);
    }
}

$view = new IndexTest();
$view->run();

