<?php
require_once 'config/init.php';
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1.0" />
		<link rel="stylesheet" href="assets/foundation/css/foundation.min.css" />
		<link rel="stylesheet" href="assets/foundation/css/foundation-icons.css" />
		<link rel="stylesheet" href="assets/css/premiumlogin.min.css" />
		<script type="text/javascript">var PLS_config = {ajax_url : '<?php echo Config::Get('base_url').'auth/ajax.php'; ?>', recaptcha_public_key : '<?php echo Config::Get('captcha.public'); ?>'};</script>
		<script type="text/template" id="recaptcha_template"> 
			<div id="PLS_recaptcha_widget" class="panel medium-12 columns">
				<div class="captcha_img" id="recaptcha_image"></div>
				<div class="captcha_btn">
					<a class="link" href="javascript:Recaptcha.reload()">Reload</a><span class="orspan">-</span>
					<a class="recaptcha_only_if_image link" href="javascript:Recaptcha.switch_type('audio')">Listen</a>
					<a class="recaptcha_only_if_audio link" href="javascript:Recaptcha.switch_type('image')">Image</a>
					<span class="orspan">-</span>
					<a class="link" href="javascript:Recaptcha.showhelp()">Help</a>
				</div>
				<input class="captcha_input" type="text" id="recaptcha_response_field" name="recaptcha_response_field" placeholder="CAPTCHA">
			</div></script>
		<script type="text/javascript" src="assets/js/jquery-1.11.0.min.js"></script>
		<script type="text/javascript" src="assets/js/premiumlogin.min.js"></script>
		<script type="text/javascript" src="assets/foundation/js/foundation.min.js"></script>
	</head>
	<body>
		<!-- Top bar -->
		<div class="sticky">
			<nav class="top-bar" data-topbar data-options="is_hover: false">
				<ul class="title-area">
					<li class="name">
						<h1><a>EdcoreWeb</a></h1>
					</li>
					<li class="toggle-topbar menu-icon"><a>Menu</a></li>
				</ul>
				<section class="top-bar-section">
					<ul class="right">
					<?php if(!Authentication::IsLogged()):?>
						<li class="active"><a data-reveal-id="signup">SIGN UP</a></li>
						<li class="divider"></li>   
						<li><a data-reveal-id="login">LOG IN</a></li>
					<?php else: $user = Session::Get("current_user");?>
						<li class="divider"></li>
						<li class="has-dropdown">
							<a>
							<img src="<?php echo $user->GetAvatar(); ?>" style="height:30px;">
							<span><?php echo $user->Get("username"); ?></span> 
							</a>
							<ul class="dropdown">
								<li><a href="user/"><i class="fi-widget"></i> Settings</a></li>
								<?php if($user->IsAdmin()): ?> 
									<li><a href="admin/"><i class="fi-lock"></i> Admin</a></li>
								<?php endif; ?>
								<li class="divider"></li>
								<li><a href="auth/logout.php"><i class="fi-power"></i> Logout</a></li>
							</ul>
						</li>
					<?php endif; ?>   
					</ul>
				</section>
			</nav>
		</div>
		<!-- Login modal -->
		<div class="row">
			<div id="login" class="reveal-modal small" data-reveal>
				<div class="medium-12 columns">
					<h4>Log in</h4>
					<h4 class="border"></h4>
				  	<form class="ajaxform" method="POST" action="">
				  		<div class="message-box"></div>
						<input type="text" name="username" placeholder="EMAIL OR USERNAME" >
						<input type="password" name="password" placeholder="PASSWORD">
						<input id="rememberme" type="checkbox" name="rememberme">
						<label class="remember" for="rememberme">Remember me</label>
						<input type="hidden" name="action" value="login">
						<input class="button expand" type="submit" name="login" value="LOG IN">
					</form>
					<img class="or image" src="assets/img/or.png">
				</div>
				<div class="medium-4 columns">
					<a onclick='PLS.Alert($(".message-box"), 2,"Loading ...");' href="auth/connect.php?method=facebook" class="button expand buttonFacebook">FACEBOOK</a>
				</div>
				<div class="medium-4 columns">
					<a onclick='PLS.Alert($(".message-box"), 2,"Loading ...");' href="auth/connect.php?method=twitter" class="button expand buttonTwitter">TWITTER</a>
				</div>
				<div class="medium-4 columns">
					<a onclick='PLS.Alert($(".message-box"), 2,"Loading ...");' href="auth/connect.php?method=google" class="button expand buttonGoogle">GOOGLE +</a>
				</div>
				<div class="medium-12 columns links">
					<a class="link" data-reveal-id="signup">Sign Up</a><span class="orspan">-</span>
					<a class="link" data-reveal-id="recover">Forgot Password</a><span class="orspan">-</span>
					<a class="link" data-reveal-id="reactivate">Resend Activation Email</a>
				</div>
				<a class="close-reveal-modal">&#215;</a>
			</div>
		</div>
		<!-- Signup modal -->
		<div class="row">
			<div id="signup" class="reveal-modal small" data-reveal>
				<div class="medium-12 columns">
					<h4>Sign up</h4>
					<h4 class="border"></h4>
				  	<form class="ajaxform" method="POST" action="">
				  		<div class="message-box"></div> 
						<input type="text" name="username" placeholder="USERNAME">
						<input type="text" name="email" placeholder="EMAIL">
						<input type="password" name="password" placeholder="PASSWORD" />
						<input type="password" name="cpassword" placeholder="CONFIRM PASSWORD" />
						<div class="PLS_recaptcha"></div>
						<input type="hidden" name="action" value="signup">
						<input class="button expand" type="submit" name="signup" value="SIGN UP">
					</form>
					<img class="or image" src="assets/img/or.png">
				</div>
				<div class="medium-4 columns">
					<a onclick='PLS.Alert($(".message-box"), 2,"Loading ...");' href="auth/connect.php?method=facebook" class="button expand buttonFacebook">FACEBOOK</a>
				</div>
				<div class="medium-4 columns">
					<a onclick='PLS.Alert($(".message-box"), 2,"Loading ...");' href="auth/connect.php?method=twitter" class="button expand buttonTwitter">TWITTER</a>
				</div>
				<div class="medium-4 columns">
					<a onclick='PLS.Alert($(".message-box"), 2,"Loading ...");' href="auth/connect.php?method=google" class="button expand buttonGoogle">GOOGLE +</a>
				</div>
				<div class="medium-12 columns links">
					<a class="link" data-reveal-id="login">Log In</a><span class="orspan">-</span>
					<a class="link" data-reveal-id="recover">Forgot Password</a><span class="orspan">-</span>
					<a class="link" data-reveal-id="reactivate">Resend Activation Email</a>
				</div>
				<a class="close-reveal-modal">&#215;</a>
			</div>
		</div>
		<!-- Forgot password modal -->
		<div class="row">
			<div id="recover" class="reveal-modal small" data-reveal>
				<div class="medium-12 columns">
					<h4>Forgot password</h4>
					<h4 class="border"></h4>
				  	<form class="ajaxform"  method="POST" action="">
						<div class="message-box"></div>
						<input type="text" name="email" placeholder="EMAIL"/>
						<div class="PLS_recaptcha"></div>
						<input type="hidden" name="action" value="recover">
						<input class="button expand" type="submit" name="recover" value="RESET PASSWORD">
					</form>
				</div>
				<div class="medium-12 columns links">
					<a class="link" data-reveal-id="login">Log In</a><span class="orspan">-</span>
					<a class="link" data-reveal-id="signup">Sign Up</a><span class="orspan">-</span>
					<a class="link" data-reveal-id="reactivate">Resend Activation Email</a>
				</div>
				<a class="close-reveal-modal">&#215;</a>
			</div>
		</div>
		<!-- Send activation email modal -->
		<div class="row">
			<div id="reactivate" class="reveal-modal small" data-reveal>
				<div class="medium-12 columns">
					<h4>Send activation email</h4>
					<h4 class="border"></h4>
				  	<form class="ajaxform" method="POST" action="">
				  		<div class="message-box"></div>
						<input type="text" name="email" placeholder="EMAIL"/>
						<div class="PLS_recaptcha"></div>
						<input type="hidden" name="action" value="reactivate">
						<input class="button expand" type="submit" name="reactivate" value="SEND EMAIL">
					</form>
				</div>
				<div class="medium-12 columns links">
					<a class="link" data-reveal-id="login">Log In</a><span class="orspan">-</span>
					<a class="link" data-reveal-id="signup">Sign Up</a><span class="orspan">-</span>
					<a class="link" data-reveal-id="recover">Forgot Password</a>
				</div>
				<a class="close-reveal-modal">&#215;</a>
			</div>
		</div>
	</body>
	<script>$(document).foundation();</script>
</html>