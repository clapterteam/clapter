<!DOCTYPE html>
<html>
<head>
	<title>Clapter</title>
	<meta name="description" content="">
	<meta name="keywords" content="">
	<meta content="text/html; charset=utf-8" http-equiv="Content-Type">
	<link type="text/css" href="style.css" rel="stylesheet">
	<!--<link href="css/ui.css" rel="stylesheet" type="text/css" />
	<link href="css/login.css" rel="stylesheet" type="text/css" />
	<link href="css/formstyler/jquery.formstyler.css" rel="stylesheet" type="text/css" />-->
	
	<?php //Стили личного кабинета, для использования шапки ?> 
	<link href='http://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900' rel='stylesheet'
          type='text/css'/>
    <link type="text/css" rel="stylesheet" href="assets/css/lk/bootstrap.css?12042015119"/>
    <link type="text/css" rel="stylesheet" href="assets/css/lk/packed.css?12042015119"/>
    <link type="text/css" rel="stylesheet" href="assets/css/lk/material-design-iconic-font.min.css?12042015119"/>
	<link type="text/css" rel="stylesheet" href="assets/css/lk/font-awesome.min.css?12042015119"/>
	<?php //конец стилей личного кабинета ?>
</head>
<body class="menubar-hoverable header-fixed menubar-pin">
<header id="header" class="fix-width">
	<div class="headerbar">
        <div class="headerbar-left">
            <ul class="header-nav header-nav-options">
                <li class="header-nav-brand">
                    <div class="brand-holder">
                        <a href="">
                            <img src="assets/img/logo.png">
                        </a>
                    </div>
                </li>
                <li>
                    <a class="btn btn-icon-toggle menubar-toggle" data-toggle="menubar" href="javascript:void(0);">
                        <i class="fa fa-bars"></i>
                    </a>
                </li>
            </ul>
            <div class="collapsed-top-menu">
                <a class="btn btn-raised ink-reaction btn-default-bright pull-right" href="#offcanvas-top-menu" data-toggle="offcanvas">
                    Меню
                    <i class="fa fa-bars"></i>
                </a>
            </div>
        </div>
        <div class="headerbar-right login-buttons">
			<button type="button" class="btn btn-default-bright" data-toggle="modal" data-target="#formModal">Вход</button>
			<button class="btn btn-default-bright" data-toggle="modal" data-target="#textModal">Регистрация</button>
        </div>
		
		
		
        <ul class="header-nav top-menu-lk-client clearfix">
            <li>
				<a href="/archive.php" style="border-bottom: 1px solid #EDEDED" title="">Владельцам автотранспорта</a>
			</li>
			<li>
				<a href="/catalog.php" style="border-bottom: 1px solid #EDEDED" title="">Магазинам запчастей</a>
			</li>
			<li>
				<a href="/request_vin.php" style="border-bottom: 1px solid #EDEDED" title="">Сервисным центрам</a>
			</li>						
			<li>
				<a href="/help.php" style="border-bottom: 1px solid #EDEDED" title="">Авторазборкам</a>
			</li>
        </ul>
    </div>
</header>
	
<div id="base">
    <div class="offcanvas">
    </div>
    <div class="offcanvas">
        <div id="offcanvas-top-menu" class="offcanvas-pane width-6">
            <div class="offcanvas-head">
                <header>Меню</header>
                <div class="offcanvas-tools">
                    <a class="btn btn-icon-toggle btn-default-light pull-right" data-dismiss="offcanvas">
                        <i class="md md-close"></i>
                    </a>
                </div>
            </div>
            <div class="offcanvas-body">
                <ul class="list-unstyled">
                    <li>
						<a href="/archive.php" title="">Архив поиск</a>
					</li>
					<li>
						<a href="/catalog.php" title="">Каталоги</a>
					</li>
					<li>
						<a href="/request_vin.php" title="">Запрос по VIN</a>
					</li>						
					<li>
						<a href="/lk/" onClick="_loginBlock();">Кабинет директора</a>
					</li>
					<li>
						<a href="/help.php" title="">Помошь по сайту</a>
					</li>
					<li>
						<a href="/feedback.php" title="">Контактная информация</a>
					</li>
                </ul>
            </div>
        </div>
    </div>
	<div id="content" class="wrapper">
		<div class="head clear">
			<ul class="top-links-box right">
			    <li>
					<div class="top-links grey login">
						<a class="btn btn-link btn-block" data-reveal-id="login"><span>Опубликовать прайс-лист</span></a>
					</div>
				</li>
				<li>
					<div class="top-links grey registration">
						<a class="btn btn-link btn-block" data-reveal-id="login"><span>Разместить свои объявления</span></a>
					</div>
				</li>
			</ul>
			
			<ul class="top-links-box left">
				<li>
					<div class="top-links purple document">
						<a class="btn btn-link btn-block" href="/request_vin.php"><span>Запрос запчастей по VIN коду</span></a>
					</div>
				</li>
				<li>
					<div class="top-links purple price">
						<a class="btn btn-link btn-block" href="/catalog.php"><span>Поиск запчастей по каталогу</span></a>
					</div>
				</li>
			</ul>
		</div>
		
		<div class="body">
		
			<div style="padding:275px 0 0;"></div>
			
			<div class="search-box">
				<form method="get" action="serch_frm.php">
					<div class="textboxsearch">	
						<input id="search" class="search" type="text" name="search" value="" autocomplete="off" placeholder="Номер или наименование запчасти" required>
						<button id="btn-search" class="btn-search"><span>Найти</span></button>
					</div>
				
					<div class="filter-box">
						<input class="styler" id="MagBase" type="radio" name="filter" value="1" checked>
						<label class="filter-item" for="MagBase"><i></i>Поиск по базе магазинов</label>

						<input class="styler" id="RazBase" type="radio" name="filter" value="2">
						<label class="filter-item" for="RazBase"><i></i>Поиск по базе разборки</label>

						<input class="styler" id="UseBase" type="radio" name="filter" value="3">
						<label class="filter-item" for="UseBase"><i></i>Поиск по базе б/у</label>
						<!--<label class="filter-item">
							<input class="styler" type="radio" name="filter" value="4">
							Б/у
						</label>-->
						<script>
						;$(function(){
							$('input.styler').styler();
						});
						</script>
					</div>
				</form>
			</div>
			<div class="add-box clear">
				<div class="add-title"><span>Подать объявление</span></div>
				
				<div class="add-column-box clear">
					<div class="add-column">
						<a class="add-item" href="#" title="" onClick="_loginBlock();">Хочу продать</a>
						<i>Если вы хотите продать</i>
					</div>
					<div class="add-column">
						<a class="add-item" href="#" title="" onClick="_loginBlock();">Хочу купить</a>
						<i>Если вы ждете предложение</i>
					</div>
					<div class="add-column">
						<a class="add-item" href="/forms.php" title="">Требуются работы</a>
						<i>Оставьте заявку если вам  требуется ремонт или иные работы</i>
					</div>
				</div>
			</div>
		</div>
		<div class="footer">
			<div class="column-box">
				<div class="column">
					<h3 class="blue"><a href="">Новости</a></h3>
					<ul>
						<li>
							<a href="#">Скидки на запчасти BMW</a>
						</li>
						<li>
							<a href="#">Новый раздел сайта</a>
						</li>
						<li>
							<a href="#">50% на амортизаторы Kayaba</a>
						</li>
					</ul>
				</div>
				<div class="column">
					<h3 class="green"><a href="">Информация о сайте</a></h3>
					<ul>
						<li>
							<a href="#">Размещение рекламы</a>
						</li>
						<li>
							<a href="#">Автосалонам</a>
						</li>
						<li>
							<a href="#">Информеры</a>
						</li>
						<li>
							<a href="#">Обратная связь</a>
						</li>
						<li>
							<a href="javascript:void(0)" onclick="agreement()">Пользовательское соглашение</a>
						</li>
						<li>
							<a href="#">Использование материалов сайта</a>
						</li> 
					</ul>
				</div>
				<div class="column">
					<h3 class="purple"><a href="">Разделы сайта</a></h3>
					<ul>
						<a href="#">
							<li>Поиск</li>
						</a>
						<li>
							<a href="#">Поиск автозапчастей</a>
						</li>
						<li>
							<a href="#">Поиск мотозапчастей</a>
						</li>
						<li>
							<a href="#">Запрос ремонта</a>
						</li>
						<li>
							<a href="#">Магазины запчастей</a>
						</li>
						<li>
							<a href="#">Как работать с сайтом</a>
						</li>
						<li>
							<a href="#">Контакты</a>
						</li>
					</ul>
				</div>
			</div>

			<div class="bottom">
				<div class="copy">&copy; 2014 Clapter.ru</div>
				
				<ul class="soc">
					<li>
						<a class="fb" href="#" target="_blank" title="Facebook"></a>
					</li>
					<li>
						<a class="tw" href="#" target="_blank" title="Tweeter"></a>
					</li>
					<li>
						<a class="in" href="#" target="_blank" title="Linkedin"></a>
					</li>
				</ul>
			</div>
		</div>
		
	</div>

	<div id="menubar" class="menubar-inverse">
        <div class="menubar-fixed-panel">
            <div>
                <a class="btn btn-icon-toggle btn-default menubar-toggle" data-toggle="menubar"
                   href="javascript:void(0);">
                    <i class="fa fa-bars"></i>
                </a>
            </div>
        </div>
        <div class="menubar-scroll-panel side-menu">
            <ul id="main-menu" class="gui-controls">
				<li>
					<a href="/index.php" title="">
					<div class="gui-icon"><i class="fa fa-folder-open fa-fw"></i></div>
					<span class="title no-wrap">Поиск запчастей</span>
					</a>
				</li>
				<li>
					<a href="/catalog.php" title="">
					<div class="gui-icon"><i class="fa fa-folder-open fa-fw"></i></div>
					<span class="title no-wrap">Каталоги</span>
					</a>
				</li>
				<li>
					<a href="/request_vin.php" title="">
					<div class="gui-icon"><i class="fa fa-folder-open fa-fw"></i></div>
					<span class="title no-wrap">Биржа запросов</span>
					</a>
				</li>
				<li>
					<a href="/forms.php" title="">
					<div class="gui-icon"><i class="fa fa-folder-open fa-fw"></i></div>
					<span class="title no-wrap">VIN Декодер</span>
					</a>
				</li>
				<li>
					<a href="/request_vin.php" title="">
					<div class="gui-icon"><i class="fa fa-folder-open fa-fw"></i></div>
					<span class="title no-wrap">Новостной портал</span>
					</a>
				</li>
				<li>
					<a href="/help.php" title="">
					<div class="gui-icon"><i class="fa fa-folder-open fa-fw"></i></div>
					<span class="title no-wrap">Форум</span>
					</a>
				</li>
				<li>
					<a href="/feedback.php" title="">
					<div class="gui-icon"><i class="fa fa-folder-open fa-fw"></i></div>
					<span class="title no-wrap">Помощь</span>
					</a>
				</li>
				
            </ul>
            <div class="menubar-foot-panel">
                <small class="no-linebreak hidden-folded">
                    <span class="opacity-75" style="border-top: 1px solid #58226e;">Copyright &copy; 2015 <a href="">www.clapter.ru</a></span>
                </small>
            </div>
        </div>
    </div>
	
</div>


<div class="modal fade" id="formModal" tabindex="-1" role="dialog" aria-labelledby="formModalLabel" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				<h4 class="modal-title" id="formModalLabel">Вход в личный кабинет</h4>
			</div>
			<form class="form-horizontal" role="form">
				<div class="modal-body">
					<div class="form-group">
						<div class="col-sm-3">
							<label for="email1" class="control-label">Электронная почта: </label>
						</div>
						<div class="col-sm-9">
							<input type="email" name="email1" id="email1" class="form-control" placeholder="Email">
						</div>
					</div>
					<div class="form-group">
						<div class="col-sm-3">
							<label for="password1" class="control-label">Пароль:</label>
						</div>
						<div class="col-sm-9">
							<input type="password" name="password1" id="password1" class="form-control" placeholder="Password">
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
					<button type="button" class="btn btn-primary">Войти</button>
				</div>
			</form>
		</div>
	</div>
</div>


</body>




<script>$(document).foundation();</script>

<?php //Скрипты личного кабинета ?> 

<script src="../../static/js/libs/jquery/jquery-1.11.2.min.js"></script>
<script src="../../static/js/libs/bootstrap/bootstrap.min.js"></script>
<script src="../../static/js/core/source/main.min.js"></script>	

</html>