<?php

function v(&$v, $def='') {
    return (isset($v) ? $v : $def);
}
function int_v(&$v, $def = 0) {
	return intval(v($v, $def));
}

/**
 * @return int
 */
function is_bot() {
	return  preg_match('/(bot|spider|crawler)/i', @$_SERVER['HTTP_USER_AGENT']);
}

function is_upload_supported() {
	return  preg_match('/(iPad|iPhone)/i', @$_SERVER['HTTP_USER_AGENT']);
}

function is_mobile() {
	return preg_match('/iPad|iPhone|Mobile|Opera\ Mini|Opera\ Mobi|Android|Blackberry|SymbianOS|GoBrowser|Minimo|NetFront/', $_SERVER['HTTP_USER_AGENT']);
}
