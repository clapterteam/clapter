<?php

class CLView extends Pimple {
    private $_runBefore = array();
    private $_runAfter = array();

    public function __construct() {
        $view = $this;

        $this['twig.templates'] = array(dirname(__FILE__).'/../templates');
        $this['twig.auto_reload'] = true;
        $this['twig.cache'] = dirname(__FILE__).'/../cache/templates';
        $this['debug'] = C_DEBUG;
        $this['twig'] = $this->share(function() use ($view) {
            return $view->_twigInit();
        });
        $view['is_loggedin'] = false;
    }

    public function addBefore($callback) {
        $this->_runBefore[] = $callback;
    }
    public function addAfter($callback) {
        $this->_runAfter[] = $callback;
    }

    public static function siteurl($url) {
        $query_array = array();
        $url_array = split_url($url, false);

        if (isset($url_array['query'])) {
            parse_str($url_array['query'], $query_array);
        }
        if (SID != '') {
            list($sid_name, $sid_val) = explode('=', SID);
            $query_array[$sid_name] = $sid_val;
        }
        if (isset($url_array['fragment'])) {
            $query_array['_t'] = microtime(true);
        }
        $url_array['query'] = http_build_query($query_array);
        return join_url($url_array, false);
    }

    public static function seturlparam($url, $param, $value=null) {
        $query_array = array();
        $url_array = split_url($url, false);

        if (isset($url_array['query'])) {
            parse_str($url_array['query'], $query_array);
        }
        if (is_null($value)) {
            unset($query_array[$param]);
        } else {
            $query_array[$param] = $value;
        }
        $url_array['query'] = http_build_query($query_array);
        return join_url($url_array, false);
    }

    public static function PLS_config() {
        $ajax_url = Config::Get('base_url').'auth/ajax.php';
        $recaptcha_public_key = Config::Get('captcha.public');
        return "var PLS_config = {ajax_url : '".$ajax_url."', recaptcha_public_key : '".$recaptcha_public_key."'};";
    }

    public function _twigInit() {
        require_once(dirname(__FILE__) . '/../lib/Twig/Autoloader.php');
        Twig_Autoloader::register();

        $loader = new Twig_Loader_Filesystem($this['twig.templates']);
        $_twig = new Twig_Environment($loader, array(
            'cache' => $this['twig.cache'],
            'charset'=>'UTF-8',
            'debug'=>$this['debug'],
            'auto_reload'=>$this['twig.auto_reload']
        ));
        if ($this['debug']) {
            $_twig->addExtension(new Twig_Extension_Debug());
        }

        $_twig->addFunction(new Twig_SimpleFunction('siteurl', function ($url) {
            return CLView::siteurl($url);
        }));

        $_twig->addFilter(new Twig_SimpleFilter('seturlparam', function ($url, $param, $value=null) {
            return CLView::seturlparam($url, $param, $value);
        }));

        $_twig->addFunction(new Twig_SimpleFunction('PLS_config', function () {
            return CLView::PLS_config();
        }));

        if (defined("SID") && !array_key_exists(session_name(), $_COOKIE)) {
            $_twig->addGlobal('SID', SID);
        }
        $_twig->addGlobal('GLOBALS', $GLOBALS);
        $_twig->addGlobal('_SERVER', $_SERVER);
        $_twig->addGlobal('USER_ROLES', CLUserRole::getUserRoles());
        return $_twig;
    }

    public function isAjax() {
        return isset($_SERVER['HTTP_X_REQUESTED_WITH']);
    }

    public function render($template, $ctx=array()) {
        return $this['twig']->render($template, $ctx);
    }

    /**
     * @param $ex Exception
     * @return string
     */
    public function otherExceptionHandler($ex) {
        $ctx = array('message'=>$ex->getMessage(), 'code'=>$ex->getCode());
        return $this['twig']->render('exception.html.twig', $ctx);
    }

    public function customExceptionHandler(CLException $ex) {
        $ctx = array('exception'=>$ex);
        if ($ex->getCode() == CLException::NOT_LOGGED_IN) {
            header("Location: ".self::siteurl("/index.php?error=must_login"));
        }
        return $this['twig']->render('ikexception.html.twig', $ctx);    }

    function run() {
        try {
            $this->before();
            $response = $this->dispatch();
            echo $this->after($response);
        } catch (CLException $ex) {
            echo $this->customExceptionHandler($ex);
        } catch (Exception $ex) {
            echo $this->otherExceptionHandler($ex);
        }
    }


    function before() {
        foreach($this->_runBefore as $c) {
            call_user_func($c, $this);
        }
    }

    function after($response) {
        foreach($this->_runAfter as $c) {
            $response = call_user_func($c, $this, $response);
        }
        return $response;
    }

    function dispatch() { return ''; }
}

class SiteFilters {
    public static function addMe($view) {
        if (isLoggedIn()) {
            $me = Session::Get('current_user');
            if (!$me) {
                throw new Exception("Logged in user not found");
            } else {
                $view['loggedin'] = $me;
                $view['is_loggedin'] = true;
                $view['twig']->addGlobal('loggedin', $me);
            }
        } else {
            $view['is_loggedin'] = false;
        }
    }

    public static function loginRequired($view) {
        if (!isLoggedIn()) {
            throw new CLException("You must be logged in!", CLException::NOT_LOGGED_IN);
        }
    }
}

class CLPublicPage extends CLView {
    public function __construct() {
        parent::__construct();
        $this->addBefore("SiteFilters::addMe");
    }
}

class CLMembersPage extends CLPublicPage
{
    public function __construct()
    {
        parent::__construct();

        $this->addBefore("SiteFilters::loginRequired");
        $this->addBefore("SiteFilters::addMe");
    }
}
