<?php
class AutoloaderInit
{
    private static $loader;

    public static function getLoader()
    {
        if (null !== self::$loader) {
            return self::$loader;
        }
        require_once __DIR__ . DIRECTORY_SEPARATOR.'AutoloadClassLoader.php';

        self::$loader = $loader = new AutoloadClassLoader();

        $classMap = require __DIR__ . DIRECTORY_SEPARATOR.'autoload_classmap.php';
        if ($classMap) {
            $loader->addClassMap($classMap);
        }

        $loader->register(true);

        return $loader;
    }
}