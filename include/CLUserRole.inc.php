<?php
class CLUserRole {
    const U_BUYER = 1;
    const U_SELLER = 2;
    const U_COMPANY = 3;
    const A_SUPER = 10;
    const A_SEO = 11;

    public static function getUserRoles() {
        return array(CLUserRole::U_BUYER => 'Покупатель', CLUserRole::U_SELLER => 'Продавец', CLUserRole::U_COMPANY => 'Компания');
    }
}
