<?php
require_once '../include/config.inc.php';

function set_foto($value, $field, $priimary_key, $list, $xcrud) {
    return '<img src="/getdata.php?id='.$value.'">';
}
class IndexAllUsers extends CLMembersPage {
    private function prepare_ctx_seller($ctx) {
        $id_user=$this['loggedin']->Get('id');
        $xcrud = Xcrud::get_instance();
        $xcrud->table('tovar');
        $xcrud->where('id_user =', $id_user);
        $xcrud->unset_print();
        $xcrud->unset_csv();
        //$xcrud->unset_remove();
        //$xcrud->unset_edit();
        $xcrud->unset_add();
        //$xcrud->unset_view();
        $xcrud->unset_numbers();
        $xcrud->columns('brand,artikle,nomenclature,availability,Shipping,price');
        $xcrud->fields('brand,artikle,nomenclature,availability,Shipping,price');
        $xcrud->table_name('Товары','Это товары которые вы импортировали из Excel');
        $xcrud->highlight('availability', '<', 1, 'red');
        $xcrud->highlight('availability', '=', 1, '#8DED79');
        $xcrud->label(array("activity" => "Обновлен","phone" => "Телефон", "brand" => "Производитель", "price" => "Цена", "artikle" => "Артикул", "availability" => "Наличие","Shipping" => "Доставка","nomenclature" => "Описание","username" => "Продавец"));
        //********************
        $xcrud2 = xcrud::get_instance();
        $xcrud2->table('tovar_full');
        $xcrud2->where('id_user =', $id_user);
        $xcrud2->unset_print();
        $xcrud2->unset_csv();
        //$xcrud2->unset_remove();
        //$xcrud2->unset_edit();
        $xcrud2->unset_add();
        //$xcrud2->unset_view();
        $xcrud2->unset_numbers();
        $xcrud2->table_name('Товары','Это товары которые вы добавили вручную');
        $xcrud2->label(array("name" => "Название","opisanie" => "Описание", "foto" => "Фото", "price" => "Цена", "artikle" => "Артикул", "model" => "Модель"
        ,"kuzov" => "Кузов", "type" => "Тип", "rest" => "Рестайлинг", "sost" => "Состояние", "yar" => "Год", "color" => "Цвет"
        ));
        $xcrud2->columns('id_user', true);
        $xcrud2->fields('id_user,foto', true);
        $xcrud2->column_callback ('foto','set_foto');
        //$xcrud2->change_type('price','price','',array('prefix'=>'$'));
        //**************************************************************
        $xcrud3 = xcrud::get_instance();
        $xcrud3->table('zapros_vin');
        $xcrud3->unset_print();
        $xcrud3->unset_csv();
        $xcrud3->unset_remove();
        $xcrud3->unset_edit();
        $xcrud3->unset_add();
        $xcrud3->unset_search();
        $xcrud3->unset_view();
        $xcrud3->unset_numbers();
        $xcrud3->unset_title();
        $xcrud3->button('#');
        $xcrud3->limit(5);
        $xcrud3->unset_limitlist();
        $xcrud3->columns('vin,model');
        $xcrud3->table_name('Запросы по VIN','Тут отображен список запросов по VIN коду');
        $xcrud3->label(array("activity" => "Обновлен","users.phone" => "Телефон", "brand" => "Производитель", "price" => "Цена", "artikle" => "Артикул", "availability" => "Наличие","Shipping" => "Доставка","nomenclature" => "Описание","users.username" => "Продавец"));

        //*********ВЫВОД ЗАКАЗОВ*********
        $xcrud4 = xcrud::get_instance();
        $xcrud4->table('zakaz');
        $xcrud4->join('id_tovar','tovar','id_tovar');
        $xcrud4->where('activ=',0);
        $xcrud4->unset_print();
        $xcrud4->unset_csv();
        $xcrud4->unset_remove();
        $xcrud4->unset_edit();
        $xcrud4->unset_add();
        $xcrud4->unset_search();
        //$xcrud4->unset_view();
        $xcrud4->unset_numbers();
        $xcrud4->unset_title();
        $xcrud4->button('javascript:ajaxSaleCompletedClick(this,{id})','','glyphicon glyphicon-ok');
        $xcrud4->limit(10);
        $xcrud4->unset_limitlist();
        $xcrud4->columns('tovar.nomenclature');
        $xcrud4->fields('name,last_name,numbers,city,phone,email,other,data_zakaz,summa,tovar.brand,tovar.artikle,tovar.nomenclature');
        //$xcrud4->table_name('Запросы по VIN','Тут отображен список запросов по VIN коду');
        $xcrud4->label(array("data_zakaz" => "Дата","phone" => "Телефон", "tovar.brand" => "Производитель", "summa" => "Цена", "tovar.artikle" => "Артикул", "other" => "Примечания","city" => "Адресс","tovar.nomenclature" => "Описание","numbers" => "Количество"));
        //*******************************
        //Выодим прайсы//
        $xcrud5 = xcrud::get_instance();
        $xcrud5->table('prices');
        // подготавливаем стстистику
//*******************************
        $user_id = $this['loggedin']->Get("id");
        $sql = "SELECT COUNT(*) FROM st_phone_clicks WHERE user_id = $user_id AND dt >= DATE_SUB(CURRENT_DATE, INTERVAL 1 MONTH) AND dt < DATE_ADD(CURRENT_DATE, INTERVAL 1 DAY)";
        $res = mysql_query($sql);
        $row = mysql_fetch_row($res);
        $phone_clicks_month_count = $row[0];

        $sql = "SELECT COUNT(*), sum(summa) FROM zakaz WHERE id_user = $user_id AND data_zakaz >= DATE_SUB(CURRENT_DATE, INTERVAL 1 MONTH) AND data_zakaz < DATE_ADD(CURRENT_DATE, INTERVAL 1 DAY)";
        $res = mysql_query($sql);
        $row = mysql_fetch_row($res);
        $zakaz_month_count = $row[0];
        $zakaz_month_total_price = $row[1];

        $sql = "SELECT COUNT(*) FROM st_favorite_clicks WHERE user_id = $user_id AND dt >= DATE_SUB(CURRENT_DATE, INTERVAL 1 MONTH) AND dt < DATE_ADD(CURRENT_DATE, INTERVAL 1 DAY)";
        $res = mysql_query($sql);
        $row = mysql_fetch_row($res);
        $fav_clicks_month_count = $row[0];

        $sql = "SELECT COUNT(*) FROM st_search_clicks WHERE user_id = $user_id AND dt >= DATE_SUB(CURRENT_DATE, INTERVAL 1 MONTH) AND dt < DATE_ADD(CURRENT_DATE, INTERVAL 1 DAY)";
        $res = mysql_query($sql);
        $row = mysql_fetch_row($res);
        $search_clicks_month_count = $row[0];

        $sql = "SELECT COUNT(*) FROM st_counter_visitor WHERE dt >= DATE_SUB(CURRENT_DATE, INTERVAL 1 MONTH) AND dt < DATE_ADD(CURRENT_DATE, INTERVAL 1 DAY)";
        $res = mysql_query($sql);
        $row = mysql_fetch_row($res);
        $visitor_total_month = $row[0];

        $sql = "SELECT COUNT(*) FROM st_counter_visitor";
        $res = mysql_query($sql);
        $row = mysql_fetch_row($res);
        $visitor_total = $row[0];

        $sql = "select count(*) from (SELECT distinct ip FROM st_counter_visitor as st group by ip) as st2";
        $res = mysql_query($sql);
        $row = mysql_fetch_row($res);
        $visitor_unique = $row[0];

        $sql = "SELECT dt FROM st_search_clicks WHERE user_id = $user_id order by dt desc limit 1";
        $res = mysql_query($sql);
        $row = mysql_fetch_row($res);
        $search_clicks_last = $row[0];

        $sql = "SELECT count(*) FROM tovar WHERE id_user = $user_id AND moderation=0";
        $res = mysql_query($sql);
        $row = mysql_fetch_row($res);
        $tovar_not_moderate = $row[0];

        $sql = "SELECT count(*) FROM tovar WHERE id_user = $user_id AND moderation=1";
        $res = mysql_query($sql);
        $row = mysql_fetch_row($res);
        $public_position = $row[0];


        $ctx['result_tab1'] = $xcrud->render();
        $ctx['result_tab2'] = $xcrud2->render();
        $ctx['result_tab3'] = $xcrud3->render();
        $ctx['result_tab4'] = $xcrud4->render();
        $ctx['result_tab5'] = $xcrud5->render();

        $ctx['phone_clicks_month_count'] = $phone_clicks_month_count;
        $ctx['zakaz_month_count'] = $zakaz_month_count;
        $ctx['zakaz_month_total_price'] = $zakaz_month_total_price;
        $ctx['fav_clicks_month_count'] = $fav_clicks_month_count;
        $ctx['search_clicks_month_count'] = $search_clicks_month_count;

        $ctx['visitor_total_month'] = $visitor_total_month;
        $ctx['visitor_total'] = $visitor_total;
        $ctx['visitor_unique'] = $visitor_unique;
        $ctx['search_clicks_last'] = $search_clicks_last;
        $ctx['tovar_not_moderate'] = $tovar_not_moderate;
        $ctx['public_position'] = $public_position;
        return $ctx;
    }

    private function prepare_ctx_buyer($ctx) {
        $xcrud = Xcrud::get_instance();
        $xcrud->table('zakaz');
        $xcrud->join('id_tovar','tovar','id_tovar');
        $xcrud->where('id_client =', $this['loggedin']->Get('id'));

        $xcrud->unset_numbers();
        $xcrud->unset_print();
        $xcrud->unset_csv();
        $xcrud->unset_add();
        $xcrud->unset_limitlist();
        $xcrud->columns('id, data_zakaz, tovar.brand, other, activ');
        $xcrud->table_name(' ');
        $xcrud->label(array("data_zakaz" => "Дата","other" => "Описание запроса", "tovar.brand" => "Марка", "activ" => "Статус"));
        //            $xcrud->column_callback ('foto_id','set_foto');

        $xcrud2 = Xcrud::get_instance();
        $xcrud2->table('zapros_vin');
        $xcrud2->where('user_id =', $this['loggedin']->Get('id'));

        $xcrud2->unset_numbers();
        $xcrud2->unset_print();
        $xcrud2->unset_csv();
        $xcrud2->unset_add();
        $xcrud2->unset_limitlist();
        $xcrud2->columns('id, dt, brand, model, comments, state');
        $xcrud2->table_name(' ');
        $xcrud2->label(array("dt" => "Дата","comments" => "Описание запроса", "brand" => "Марка", "model" => "Модель", "state" => "Состояние"));

        $ctx['result_tab1'] = $xcrud->render();
        $ctx['result_tab2'] = $xcrud2->render();

        return $ctx;
    }

    public function dispatch() {
        $ctx = array();
        if ($this['loggedin']->Get('user_role') == CLUserRole::U_SELLER) {
            $ctx = $this->prepare_ctx_seller($ctx);
            return $this->render('workspace/seller.html.twig', $ctx);
        }
        if ($this['loggedin']->Get('user_role') == CLUserRole::U_BUYER) {
            $ctx = $this->prepare_ctx_buyer($ctx);
            return $this->render('workspace/buyer.html.twig', $ctx);
        }
        return $this->render('workspace/index.html.twig', $ctx);
    }
}

$view = new IndexAllUsers();
$view->run();
