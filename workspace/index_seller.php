<?php
require_once 'include/config.inc.php';
class Sellers extends CLPublicPage {
    public function dispatch() {

        $user = Session::Get("current_user");
        $id_user=$user->Get("id");
        include('xcrud/xcrud.php');
        $xcrud = Xcrud::get_instance();
        $xcrud->table('tovar');
        $xcrud->where('id_user =', $id_user);
        $xcrud->unset_print();
        $xcrud->unset_csv();
        //$xcrud->unset_remove();
        //$xcrud->unset_edit();
        $xcrud->unset_add();
        //$xcrud->unset_view();
        $xcrud->unset_numbers();
        $xcrud->columns('brand,artikle,nomenclature,availability,Shipping,price');
        $xcrud->fields('brand,artikle,nomenclature,availability,Shipping,price');
        $xcrud->table_name('Товары','Это товары которые вы импортировали из Excel');
        $xcrud->highlight('availability', '<', 1, 'red');
        $xcrud->highlight('availability', '=', 1, '#8DED79');
        $xcrud->label(array("activity" => "Обновлен","phone" => "Телефон", "brand" => "Производитель", "price" => "Цена", "artikle" => "Артикул", "availability" => "Наличие","Shipping" => "Доставка","nomenclature" => "Описание","username" => "Продавец"));
        //********************
        $xcrud2 = xcrud::get_instance();
        $xcrud2->table('tovar_full');
        $xcrud2->where('id_user =', $id_user);
        $xcrud2->unset_print();
        $xcrud2->unset_csv();
        //$xcrud2->unset_remove();
        //$xcrud2->unset_edit();
        $xcrud2->unset_add();
        //$xcrud2->unset_view();
        $xcrud2->unset_numbers();
        $xcrud2->table_name('Товары','Это товары которые вы добавили вручную');
        $xcrud2->label(array("name" => "Название","opisanie" => "Описание", "foto" => "Фото", "price" => "Цена", "artikle" => "Артикул", "model" => "Модель"
        ,"kuzov" => "Кузов", "type" => "Тип", "rest" => "Рестайлинг", "sost" => "Состояние", "yar" => "Год", "color" => "Цвет"
        ));
        $xcrud2->columns('id_user', true);
        $xcrud2->fields('id_user,foto', true);
        $xcrud2->column_callback ('foto','set_foto');
        //$xcrud2->change_type('price','price','',array('prefix'=>'$'));
        function set_foto($value, $field, $priimary_key, $list, $xcrud)
        {
            return '<img src="getdata.php?id='.$value.'">';
        }
        //***************
        //**************************************************************
        $xcrud3 = xcrud::get_instance();
        $xcrud3->table('zapros_vin');
        $xcrud3->unset_print();
        $xcrud3->unset_csv();
        $xcrud3->unset_remove();
        $xcrud3->unset_edit();
        $xcrud3->unset_add();
        $xcrud3->unset_search();
        $xcrud3->unset_view();
        $xcrud3->unset_numbers();
        $xcrud3->unset_title();
        $xcrud3->button('#');
        $xcrud3->limit(5);
        $xcrud3->unset_limitlist();
        $xcrud3->columns('vin,model');
        $xcrud3->table_name('Запросы по VIN','Тут отображен список запросов по VIN коду');
        $xcrud3->label(array("activity" => "Обновлен","users.phone" => "Телефон", "brand" => "Производитель", "price" => "Цена", "artikle" => "Артикул", "availability" => "Наличие","Shipping" => "Доставка","nomenclature" => "Описание","users.username" => "Продавец"));

//*********ВЫВОД ЗАКАЗОВ*********
        $xcrud4 = xcrud::get_instance();
        $xcrud4->table('zakaz');
        $xcrud4->join('id_tovar','tovar','id_tovar');
        $xcrud4->where('activ=',0);
        $xcrud4->unset_print();
        $xcrud4->unset_csv();
        $xcrud4->unset_remove();
        $xcrud4->unset_edit();
        $xcrud4->unset_add();
        $xcrud4->unset_search();
        //$xcrud4->unset_view();
        $xcrud4->unset_numbers();
        $xcrud4->unset_title();
        $xcrud4->button('javascript:ajaxSaleCompletedClick(this,{id})','','glyphicon glyphicon-ok');
        $xcrud4->limit(10);
        $xcrud4->unset_limitlist();
        $xcrud4->columns('tovar.nomenclature');
        $xcrud4->fields('name,last_name,numbers,city,phone,email,other,data_zakaz,summa,tovar.brand,tovar.artikle,tovar.nomenclature');
        //$xcrud4->table_name('Запросы по VIN','Тут отображен список запросов по VIN коду');
        $xcrud4->label(array("data_zakaz" => "Дата","phone" => "Телефон", "tovar.brand" => "Производитель", "summa" => "Цена", "tovar.artikle" => "Артикул", "other" => "Примечания","city" => "Адресс","tovar.nomenclature" => "Описание","numbers" => "Количество"));
//*******************************

        $ctx = array('result_tab1'=>$xcrud->render(),'result_tab2'=>$xcrud2->render(),'result_tab3'=>$xcrud3->render(),'result_tab4'=>$xcrud4->render());
        return $this->render('/workspace/seller.html.html.twig', $ctx);
    }
}

$view = new Sellers();
$view->run();