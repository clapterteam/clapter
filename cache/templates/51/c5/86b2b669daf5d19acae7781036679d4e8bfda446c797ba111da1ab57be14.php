<?php

/* ikexception.html.twig */
class __TwigTemplate_51c586b2b669daf5d19acae7781036679d4e8bfda446c797ba111da1ab57be14 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("layout/layout.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'extrascripts' => array($this, 'block_extrascripts'),
            'content' => array($this, 'block_content'),
            'actions' => array($this, 'block_actions'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        echo "Error";
    }

    // line 5
    public function block_extrascripts($context, array $blocks = array())
    {
        // line 6
        $this->displayParentBlock("extrascripts", $context, $blocks);
        echo "
<script type=\"text/javascript\">
\$(document).ready(function() {
    if (history.length > 0) {
        \$('#exception-container').append(\"<button onclick='history.go(-1)'>";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["w"]) ? $context["w"] : null), 515, array(), "array"), "html", null, true);
        echo "</button>\");
    }
});
</script>
";
    }

    // line 16
    public function block_content($context, array $blocks = array())
    {
        // line 17
        echo "<div id=\"exception-container\" style=\"padding: 1em\">
<h2>";
        // line 18
        $this->displayBlock("title", $context, $blocks);
        echo "</h2>
<p>
";
        // line 20
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : null), "message", array()), "html", null, true);
        echo "
</p>
<br>
";
        // line 23
        $this->displayBlock('actions', $context, $blocks);
        // line 26
        echo "</div>
";
    }

    // line 23
    public function block_actions($context, array $blocks = array())
    {
        // line 24
        echo "    <a target=\"_top\" class='button button-pink' href='";
        echo twig_escape_filter($this->env, call_user_func_array($this->env->getFunction('siteurl')->getCallable(), array("/")), "html", null, true);
        echo "'>Home</a>
";
    }

    public function getTemplateName()
    {
        return "ikexception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  91 => 24,  88 => 23,  83 => 26,  81 => 23,  75 => 20,  70 => 18,  67 => 17,  64 => 16,  55 => 10,  48 => 6,  45 => 5,  39 => 3,  11 => 1,);
    }
}
