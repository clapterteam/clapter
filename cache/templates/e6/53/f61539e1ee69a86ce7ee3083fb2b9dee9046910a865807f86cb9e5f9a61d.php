<?php

/* searchvinresult.html.twig */
class __TwigTemplate_e653f61539e1ee69a86ce7ee3083fb2b9dee9046910a865807f86cb9e5f9a61d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("layout/layout.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        if ( !(isset($context["loggedin"]) ? $context["loggedin"] : null)) {
            // line 5
            echo "        <a href=\"/lib/login/auth/login.php\">Log In</a>
        <a href=\"/lib/login/auth/signup.php\">Sign Up</a>
    ";
        } else {
            // line 8
            echo "        ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["loggedin"]) ? $context["loggedin"] : null), "Get", array(0 => "username"), "method"), "html", null, true);
            echo ",
        <a href=\"/lib/login/auth/logout.php\">Exit</a><br>
    ";
        }
        // line 11
        echo "<form method=\"post\" action=\"\">
    <div class=\"textboxsearch\">
        <input id=\"search\" class=\"search\" type=\"text\" name=\"search_vin\" value=\"\" autocomplete=\"off\" placeholder=\"Ваш VIN код\" required>
        <button id=\"btn-search\" class=\"btn-search\"><span>Найти</span></button>
    </div>
</form>
    Результат поиска
    ";
        // line 18
        echo (isset($context["search_vin_result"]) ? $context["search_vin_result"] : null);
        echo "
";
    }

    public function getTemplateName()
    {
        return "searchvinresult.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  63 => 18,  54 => 11,  47 => 8,  42 => 5,  39 => 4,  36 => 3,  11 => 1,);
    }
}
