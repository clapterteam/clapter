<?php

/* workspace/index.html.twig */
class __TwigTemplate_5ce450e9b4fddf516055354f991f5a952bf69b56cd577ee51c962b3c67a197f3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("layout/layout.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "    ";
        if ($this->getAttribute((isset($context["loggedin"]) ? $context["loggedin"] : null), "isAdmin", array(), "method")) {
            // line 4
            echo "        ";
            $this->env->loadTemplate("workspace/admin.html.twig")->display($context);
            // line 5
            echo "    ";
        } else {
            // line 6
            echo "        ";
            if (($this->getAttribute((isset($context["loggedin"]) ? $context["loggedin"] : null), "Get", array(0 => "user_role"), "method") == twig_constant("CLUserRole::U_BUYER"))) {
                // line 7
                echo "            ";
                $this->env->loadTemplate("workspace/buyer.html.twig")->display($context);
                // line 8
                echo "        ";
            }
            // line 9
            echo "        ";
            if (($this->getAttribute((isset($context["loggedin"]) ? $context["loggedin"] : null), "Get", array(0 => "user_role"), "method") == twig_constant("CLUserRole::U_SELLER"))) {
                // line 10
                echo "            ";
                $this->env->loadTemplate("workspace/seller.html.twig")->display($context);
                // line 11
                echo "        ";
            }
            // line 12
            echo "        ";
            if (($this->getAttribute((isset($context["loggedin"]) ? $context["loggedin"] : null), "Get", array(0 => "user_role"), "method") == twig_constant("CLUserRole::U_COMPANY"))) {
                // line 13
                echo "            ";
                $this->env->loadTemplate("workspace/company.html.twig")->display($context);
                // line 14
                echo "        ";
            }
            // line 15
            echo "    ";
        }
    }

    public function getTemplateName()
    {
        return "workspace/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  75 => 15,  72 => 14,  69 => 13,  66 => 12,  63 => 11,  60 => 10,  57 => 9,  54 => 8,  51 => 7,  48 => 6,  45 => 5,  42 => 4,  39 => 3,  36 => 2,  11 => 1,);
    }
}
