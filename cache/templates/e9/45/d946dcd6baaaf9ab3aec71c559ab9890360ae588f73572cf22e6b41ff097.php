<?php

/* layout/layout.html.twig */
class __TwigTemplate_e945d946dcd6baaaf9ab3aec71c559ab9890360ae588f73572cf22e6b41ff097 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("layout/base-layout.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'extrahead' => array($this, 'block_extrahead'),
            'extrastyles' => array($this, 'block_extrastyles'),
            'extrascripts' => array($this, 'block_extrascripts'),
            'title' => array($this, 'block_title'),
            'leftmenu' => array($this, 'block_leftmenu'),
            'header' => array($this, 'block_header'),
            'footer' => array($this, 'block_footer'),
            'lastblock' => array($this, 'block_lastblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout/base-layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_extrahead($context, array $blocks = array())
    {
        // line 4
        echo "<link rel=\"shortcut icon\" href=\"/images/favicon.ico\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
";
    }

    // line 8
    public function block_extrastyles($context, array $blocks = array())
    {
        // line 9
        echo "    <link rel=\"stylesheet\" href=\"//ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/themes/smoothness/jquery-ui.css\">
    ";
        // line 11
        echo "    <link type=\"text/css\" href=\"/css/style.css\" rel=\"stylesheet\">
    <link href='http://fonts.googleapis.com/css?family=Roboto:300italic,400italic,300,400,500,700,900' rel='stylesheet' type='text/css'/>
    <link href=\"/css/bootstrap.min.css\" rel=\"stylesheet\">
    <link type=\"text/css\" rel=\"stylesheet\" href=\"/css/packed.css\"/>
    <link type=\"text/css\" rel=\"stylesheet\" href=\"/css/material-design-iconic-font.min.css\"/>
    <link type=\"text/css\" rel=\"stylesheet\" href=\"/css/font-awesome.min.css\"/>
    <style type=\"text/css\">
        ul.header-nav.top-menu-lk-client li a {
            border-bottom: 1px solid #EDEDED;
        }
    </style>
";
    }

    // line 24
    public function block_extrascripts($context, array $blocks = array())
    {
        // line 25
        echo "    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src=\"//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>
    <script src=\"//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js\"></script>
    <![endif]-->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src=\"//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js\"></script>
    <script src=\"//ajax.googleapis.com/ajax/libs/jqueryui/1.11.3/jquery-ui.min.js\"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src=\"/js/bootstrap.min.js\"></script>
    <script src=\"/js/core/main.min.js\"></script>

    <script type=\"text/javascript\">";
        // line 38
        echo call_user_func_array($this->env->getFunction('PLS_config')->getCallable(), array());
        echo "</script>
    ";
        // line 40
        echo "        ";
        // line 41
        echo "            ";
        // line 42
        echo "            ";
        // line 43
        echo "                ";
        // line 44
        echo "                ";
        // line 45
        echo "                ";
        // line 46
        echo "                ";
        // line 47
        echo "                ";
        // line 48
        echo "            ";
        // line 49
        echo "            ";
        // line 50
        echo "        ";
        // line 51
        echo "    <script type=\"text/javascript\" src=\"/lib/login/assets/js/premiumlogin.min.js\"></script>
";
    }

    // line 54
    public function block_title($context, array $blocks = array())
    {
        echo "Clapter";
    }

    // line 56
    public function block_leftmenu($context, array $blocks = array())
    {
        // line 57
        echo "    ";
        if ( !(isset($context["loggedin"]) ? $context["loggedin"] : null)) {
            // line 58
            echo "        ";
            $this->env->loadTemplate("layout/leftmenu-common.html.twig")->display($context);
            // line 59
            echo "    ";
        } else {
            // line 60
            echo "        ";
            if ($this->getAttribute((isset($context["loggedin"]) ? $context["loggedin"] : null), "isAdmin", array(), "method")) {
                // line 61
                echo "            ";
                $this->env->loadTemplate("layout/leftmenu-admin.html.twig")->display($context);
                // line 62
                echo "        ";
            } else {
                // line 63
                echo "            ";
                if (($this->getAttribute((isset($context["loggedin"]) ? $context["loggedin"] : null), "Get", array(0 => "user_role"), "method") == twig_constant("CLUserRole::U_BUYER"))) {
                    // line 64
                    echo "                ";
                    $this->env->loadTemplate("layout/leftmenu-buyer.html.twig")->display($context);
                    // line 65
                    echo "            ";
                }
                // line 66
                echo "            ";
                if (($this->getAttribute((isset($context["loggedin"]) ? $context["loggedin"] : null), "Get", array(0 => "user_role"), "method") == twig_constant("CLUserRole::U_SELLER"))) {
                    // line 67
                    echo "                ";
                    $this->env->loadTemplate("layout/leftmenu-seller.html.twig")->display($context);
                    // line 68
                    echo "            ";
                }
                // line 69
                echo "            ";
                if (($this->getAttribute((isset($context["loggedin"]) ? $context["loggedin"] : null), "Get", array(0 => "user_role"), "method") == twig_constant("CLUserRole::U_COMPANY"))) {
                    // line 70
                    echo "                ";
                    $this->env->loadTemplate("layout/leftmenu-company.html.twig")->display($context);
                    // line 71
                    echo "            ";
                }
                // line 72
                echo "        ";
            }
            // line 73
            echo "    ";
        }
    }

    // line 76
    public function block_header($context, array $blocks = array())
    {
        // line 77
        echo "    ";
        $this->env->loadTemplate("layout/header.html.twig")->display($context);
    }

    // line 80
    public function block_footer($context, array $blocks = array())
    {
        // line 81
        echo "    ";
        $this->env->loadTemplate("layout/footer.html.twig")->display($context);
    }

    // line 84
    public function block_lastblock($context, array $blocks = array())
    {
        // line 85
        echo "    ";
        $this->env->loadTemplate("embed/regmodals.html.twig")->display($context);
    }

    public function getTemplateName()
    {
        return "layout/layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  203 => 85,  200 => 84,  195 => 81,  192 => 80,  187 => 77,  184 => 76,  179 => 73,  176 => 72,  173 => 71,  170 => 70,  167 => 69,  164 => 68,  161 => 67,  158 => 66,  155 => 65,  152 => 64,  149 => 63,  146 => 62,  143 => 61,  140 => 60,  137 => 59,  134 => 58,  131 => 57,  128 => 56,  122 => 54,  117 => 51,  115 => 50,  113 => 49,  111 => 48,  109 => 47,  107 => 46,  105 => 45,  103 => 44,  101 => 43,  99 => 42,  97 => 41,  95 => 40,  91 => 38,  76 => 25,  73 => 24,  58 => 11,  55 => 9,  52 => 8,  46 => 4,  43 => 3,  11 => 1,);
    }
}
