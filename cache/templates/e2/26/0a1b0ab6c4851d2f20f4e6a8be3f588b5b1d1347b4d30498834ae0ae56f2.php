<?php

/* index.tmpl */
class __TwigTemplate_e2260a1b0ab6c4851d2f20f4e6a8be3f588b5b1d1347b4d30498834ae0ae56f2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<html>
  <head>
  <link href=\"css/bootstrap.min.css\" rel=\"stylesheet\">
      <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
      <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
      <!--[if lt IE 9]>
        <script src=\"https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>
        <script src=\"https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js\"></script>
      <![endif]-->
  </head>
  <body>
    <div id=\"page\">
      <div id=\"header\">
      ";
        // line 14
        $this->env->loadTemplate("header.tmpl")->display($context);
        // line 15
        echo "      ";
        if ( !(isset($context["loggedin"]) ? $context["loggedin"] : null)) {
            // line 16
            echo "                  <a href=\"/lib/login/auth/login.php\">Log In</a>
                  <a href=\"/lib/login/auth/signup.php\">Sign Up</a>
          ";
        } else {
            // line 19
            echo " ";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["loggedin"]) ? $context["loggedin"] : null), "Get", array(0 => "username"), "method"), "html", null, true);
            echo ",
<a href=\"/lib/login/auth/logout.php\">Exit</a><br>
";
            // line 21
            echo (isset($context["result"]) ? $context["result"] : null);
            echo "
          ";
        }
        // line 23
        echo "      </div>

      <div id=\"left\">
        ";
        // line 26
        $this->env->loadTemplate("left_menu.tmpl")->display($context);
        // line 27
        echo "        ";
        $this->env->loadTemplate("main.tmpl")->display($context);
        // line 28
        echo "      </div>
      <div id=\"right\">
      </div>
      <div id=\"footer\">
        ";
        // line 32
        $this->env->loadTemplate("footer.tmpl")->display($context);
        // line 33
        echo "      </div>
    </div>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src=\"https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js\"></script>
        <!-- Include all compiled plugins (below), or include individual files as needed -->
        <script src=\"js/bootstrap.min.js\"></script>
  </body>
</html>";
    }

    public function getTemplateName()
    {
        return "index.tmpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 33,  71 => 32,  65 => 28,  62 => 27,  60 => 26,  55 => 23,  50 => 21,  44 => 19,  39 => 16,  36 => 15,  34 => 14,  19 => 1,);
    }
}
