<?php

/* index.html.twig */
class __TwigTemplate_d7f37f969c61f86f2caea50c0c00d7451999b4257474c2ca0b4c528bd1c17a94 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("layout/layout.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "    ";
        echo (isset($context["result"]) ? $context["result"] : null);
        echo "

    <form method=\"get\" action=\"search_result_fr.php\">
        <div class=\"textboxsearch\">
            <input id=\"search\" class=\"search\" type=\"text\" name=\"search\" value=\"\" autocomplete=\"off\" placeholder=\"Номер или наименование запчасти\" required>
            <button id=\"btn-search\" class=\"btn-search\"><span>Найти</span></button>
        </div>

        <div class=\"filter-box\">
            <input class=\"styler\" id=\"MagBase\" type=\"radio\" name=\"filter\" value=\"1\" checked>
            <label class=\"filter-item\" for=\"MagBase\"><i></i>Поиск по базе магазинов</label>

            <input class=\"styler\" id=\"RazBase\" type=\"radio\" name=\"filter\" value=\"2\">
            <label class=\"filter-item\" for=\"RazBase\"><i></i>Поиск по базе разборки</label>

            <input class=\"styler\" id=\"UseBase\" type=\"radio\" name=\"filter\" value=\"3\">
            <label class=\"filter-item\" for=\"UseBase\"><i></i>Поиск по базе б/у</label>
        </div>
    </form>
";
    }

    public function getTemplateName()
    {
        return "index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 3,  36 => 2,  11 => 1,);
    }
}
