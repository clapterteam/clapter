<?php

/* layout/leftmenu-seller.html.twig */
class __TwigTemplate_4bde7e69f94142b7cbc7d41907c9480e7846dd5f9616ebc2c137d263a157f8a4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"menubar\" class=\"menubar-inverse home-bg\">
    <div class=\"menubar-fixed-panel\">
        <div>
            <a class=\"btn btn-icon-toggle btn-default menubar-toggle\" data-toggle=\"menubar\"
               href=\"javascript:void(0);\">
                <i class=\"fa fa-bars\"></i>
            </a>
        </div>
    </div>
    <div class=\"menubar-scroll-panel side-menu\">
        <ul id=\"main-menu\" class=\"gui-controls\">
            <li>
                <a href=\"/workspace/index.php\" title=\"\">
                    <div class=\"gui-icon\"><i class=\"fa fa-folder-open fa-fw\"></i></div>
                    <span class=\"title no-wrap\">Рабочий стол</span>
                </a>
            </li>
            <li>
                <a href=\"#\" title=\"\">
                    <div class=\"gui-icon\"><i class=\"fa fa-folder-open fa-fw\"></i></div>
                    <span class=\"title no-wrap\">Публикация</span>
                </a>
            </li>
            <li>
                <a href=\"#\" title=\"\">
                    <div class=\"gui-icon\"><i class=\"fa fa-folder-open fa-fw\"></i></div>
                    <span class=\"title no-wrap\">Бренды</span>
                </a>
            </li>
            <li>
                <a href=\"#\" title=\"\">
                    <div class=\"gui-icon\"><i class=\"fa fa-folder-open fa-fw\"></i></div>
                    <span class=\"title no-wrap\">Мои услуги</span>
                </a>
            </li>
            <li>
                <a href=\"#\" title=\"\">
                    <div class=\"gui-icon\"><i class=\"fa fa-folder-open fa-fw\"></i></div>
                    <span class=\"title no-wrap\">Статистика</span>
                </a>
            </li>
            <li>
                <a href=\"#\" title=\"\">
                    <div class=\"gui-icon\"><i class=\"fa fa-folder-open fa-fw\"></i></div>
                    <span class=\"title no-wrap\">Отзывы</span>
                </a>
            </li>
            <li>
                <a href=\"#\" title=\"\">
                    <div class=\"gui-icon\"><i class=\"fa fa-folder-open fa-fw\"></i></div>
                    <span class=\"title no-wrap\">Документооборот</span>
                </a>
            </li>
            <li>
                <a href=\"#\" title=\"\">
                    <div class=\"gui-icon\"><i class=\"fa fa-folder-open fa-fw\"></i></div>
                    <span class=\"title no-wrap\">Мои данные</span>
                </a>
            </li>
            <li>
                <a href=\"#\" title=\"\">
                    <div class=\"gui-icon\"><i class=\"fa fa-folder-open fa-fw\"></i></div>
                    <span class=\"title no-wrap\">Поддержка</span>
                </a>
            </li>
        </ul>
        <div class=\"menubar-foot-panel\">
            <small class=\"no-linebreak hidden-folded\">
                <span class=\"opacity-75\" style=\"border-top: 1px solid #58226e;\">Copyright &copy; 2015 <a href=\"\">www.clapter.ru</a></span>
            </small>
        </div>
    </div>
</div>

";
        // line 76
        echo "    ";
        // line 77
        echo "        ";
        // line 78
        echo "            ";
        // line 79
        echo "            ";
        // line 80
        echo "                ";
        // line 81
        echo "                ";
        // line 82
        echo "            ";
        // line 83
        echo "        ";
        // line 84
        echo "        ";
        // line 85
        echo "            ";
        // line 86
        echo "            ";
        // line 87
        echo "                ";
        // line 88
        echo "                ";
        // line 89
        echo "            ";
        // line 90
        echo "        ";
        // line 91
        echo "        ";
        // line 92
        echo "            ";
        // line 93
        echo "            ";
        // line 94
        echo "                ";
        // line 95
        echo "                ";
        // line 96
        echo "            ";
        // line 97
        echo "        ";
        // line 98
        echo "        ";
        // line 99
        echo "            ";
        // line 100
        echo "            ";
        // line 101
        echo "                ";
        // line 102
        echo "                ";
        // line 103
        echo "            ";
        // line 104
        echo "        ";
        // line 105
        echo "    ";
    }

    public function getTemplateName()
    {
        return "layout/leftmenu-seller.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  153 => 105,  151 => 104,  149 => 103,  147 => 102,  145 => 101,  143 => 100,  141 => 99,  139 => 98,  137 => 97,  135 => 96,  133 => 95,  131 => 94,  129 => 93,  127 => 92,  125 => 91,  123 => 90,  121 => 89,  119 => 88,  117 => 87,  115 => 86,  113 => 85,  111 => 84,  109 => 83,  107 => 82,  105 => 81,  103 => 80,  101 => 79,  99 => 78,  97 => 77,  95 => 76,  19 => 1,);
    }
}
