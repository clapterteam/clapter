<?php

/* add_excel.html.twig */
class __TwigTemplate_ed8540546b67c57b68ca02fd677c127a0df5789bb1e0437eb6ac5398cca308d8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("layout/layout.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "    ";
        echo twig_escape_filter($this->env, (isset($context["col_add"]) ? $context["col_add"] : null), "html", null, true);
        echo "
    <form action=\"\" method=\"post\" enctype=\"multipart/form-data\">
        <input type=\"file\" name=\"file\" />
        <input type=\"submit\" value=\"Добавить\"/>
    </form>
";
    }

    public function getTemplateName()
    {
        return "add_excel.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  39 => 3,  36 => 2,  11 => 1,);
    }
}
