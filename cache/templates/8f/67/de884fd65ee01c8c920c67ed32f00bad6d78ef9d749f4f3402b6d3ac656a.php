<?php

/* layout/header.html.twig */
class __TwigTemplate_8f67de884fd65ee01c8c920c67ed32f00bad6d78ef9d749f4f3402b6d3ac656a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<header id=\"header\">
    <div class=\"headerbar\">
        <div class=\"headerbar-left\">
            <ul class=\"header-nav header-nav-options\">
                <li class=\"header-nav-brand\">
                    <div class=\"brand-holder\">
                        <a href=\"\">
                            <img src=\"/img/logo.png\">
                        </a>
                    </div>
                </li>
                <li>
                    <a class=\"btn btn-icon-toggle menubar-toggle\" data-toggle=\"menubar\" href=\"javascript:void(0);\">
                        <i class=\"fa fa-bars\"></i>
                    </a>
                </li>
            </ul>
            <div class=\"collapsed-top-menu\">
                <a class=\"btn btn-raised ink-reaction btn-default-bright pull-right\" href=\"#offcanvas-top-menu\"
                   data-toggle=\"offcanvas\">
                    Меню
                    <i class=\"fa fa-bars\"></i>
                </a>
            </div>
        </div>
        <div class=\"headerbar-right login-buttons\">
            ";
        // line 27
        if ( !(isset($context["loggedin"]) ? $context["loggedin"] : null)) {
            // line 28
            echo "            <button type=\"button\" class=\"btn btn-default-bright\" data-toggle=\"modal\" data-target=\"#login\">Вход</button>
            <button type=\"button\" class=\"btn btn-default-bright\" data-toggle=\"modal\" data-target=\"#signup\">Регистрация</button>
            ";
        } else {
            // line 31
            echo "            <a class=\"btn btn-default-bright\" href=\"/workspace/index.php\">Рабочий стол</a>
            <a class=\"btn btn-default-bright\" href=\"/lib/login/auth/logout.php\"><i class=\"fi-power\"></i>Выход</a>
            ";
        }
        // line 34
        echo "        </div>

        ";
        // line 36
        if ( !(isset($context["loggedin"]) ? $context["loggedin"] : null)) {
            // line 37
            echo "            ";
            $this->env->loadTemplate("layout/topmenu-common.html.twig")->display($context);
            // line 38
            echo "        ";
        } else {
            // line 39
            echo "            ";
            if ($this->getAttribute((isset($context["loggedin"]) ? $context["loggedin"] : null), "isAdmin", array(), "method")) {
                // line 40
                echo "                ";
                $this->env->loadTemplate("layout/topmenu-admin.html.twig")->display($context);
                // line 41
                echo "            ";
            } else {
                // line 42
                echo "                ";
                if (($this->getAttribute((isset($context["loggedin"]) ? $context["loggedin"] : null), "Get", array(0 => "user_role"), "method") == twig_constant("CLUserRole::U_BUYER"))) {
                    // line 43
                    echo "                    ";
                    $this->env->loadTemplate("layout/topmenu-buyer.html.twig")->display($context);
                    // line 44
                    echo "                ";
                }
                // line 45
                echo "                ";
                if (($this->getAttribute((isset($context["loggedin"]) ? $context["loggedin"] : null), "Get", array(0 => "user_role"), "method") == twig_constant("CLUserRole::U_SELLER"))) {
                    // line 46
                    echo "                    ";
                    $this->env->loadTemplate("layout/topmenu-seller.html.twig")->display($context);
                    // line 47
                    echo "                ";
                }
                // line 48
                echo "                ";
                if (($this->getAttribute((isset($context["loggedin"]) ? $context["loggedin"] : null), "Get", array(0 => "user_role"), "method") == twig_constant("CLUserRole::U_COMPANY"))) {
                    // line 49
                    echo "                    ";
                    $this->env->loadTemplate("layout/topmenu-company.html.twig")->display($context);
                    // line 50
                    echo "                ";
                }
                // line 51
                echo "            ";
            }
            // line 52
            echo "        ";
        }
        // line 53
        echo "    </div>
</header>

";
    }

    public function getTemplateName()
    {
        return "layout/header.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  113 => 53,  110 => 52,  107 => 51,  104 => 50,  101 => 49,  98 => 48,  95 => 47,  92 => 46,  89 => 45,  86 => 44,  83 => 43,  80 => 42,  77 => 41,  74 => 40,  71 => 39,  68 => 38,  65 => 37,  63 => 36,  59 => 34,  54 => 31,  49 => 28,  47 => 27,  19 => 1,);
    }
}
