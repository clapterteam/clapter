<?php

/* left_menu.tmpl */
class __TwigTemplate_15ffc3fb6c50ea780a13fb3819e1e780399eb1ab07cfa61a4f5f59f63e74a30b extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "
<div class=”left_menu”>
{*Выводим список каких-то категорий*}
{for category in category_tree}
<a href=”?{category.id}”>{category.name}</a>
     {endfor}
</div>";
    }

    public function getTemplateName()
    {
        return "left_menu.tmpl";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
