<?php

/* layout/leftmenu-buyer.html.twig */
class __TwigTemplate_937a8e6e251ccfc39727e8a3d05e9906a69173757f33c695b7aa3449f309b3d6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"menubar\" class=\"menubar-inverse home-bg\">
    <div class=\"menubar-fixed-panel\">
        <div>
            <a class=\"btn btn-icon-toggle btn-default menubar-toggle\" data-toggle=\"menubar\"
               href=\"javascript:void(0);\">
                <i class=\"fa fa-bars\"></i>
            </a>
        </div>
    </div>
    <div class=\"menubar-scroll-panel side-menu\">
        <ul id=\"main-menu\" class=\"gui-controls\">
            <li>
                <a href=\"/workspace/index.php\" title=\"\">
                    <div class=\"gui-icon\"><i class=\"fa fa-folder-open fa-fw\"></i></div>
                    <span class=\"title no-wrap\">Рабочий стол</span>
                </a>
            </li>
            <li>
                <a href=\"#\" title=\"\">
                    <div class=\"gui-icon\"><i class=\"fa fa-folder-open fa-fw\"></i></div>
                    <span class=\"title no-wrap\">Запросы</span>
                </a>
            </li>
            <li>
                <a href=\"#\" title=\"\">
                    <div class=\"gui-icon\"><i class=\"fa fa-folder-open fa-fw\"></i></div>
                    <span class=\"title no-wrap\">Заказы продавцам</span>
                </a>
            </li>
            <li>
                <a href=\"#\" title=\"\">
                    <div class=\"gui-icon\"><i class=\"fa fa-folder-open fa-fw\"></i></div>
                    <span class=\"title no-wrap\">Мои машины</span>
                </a>
            </li>
            <li>
                <a href=\"#\" title=\"\">
                    <div class=\"gui-icon\"><i class=\"fa fa-folder-open fa-fw\"></i></div>
                    <span class=\"title no-wrap\">Мои обьявления</span>
                </a>
            </li>
            <li>
                <a href=\"#\" title=\"\">
                    <div class=\"gui-icon\"><i class=\"fa fa-folder-open fa-fw\"></i></div>
                    <span class=\"title no-wrap\">Профиль</span>
                </a>
            </li>
            <li>
                <a href=\"#\" title=\"\">
                    <div class=\"gui-icon\"><i class=\"fa fa-folder-open fa-fw\"></i></div>
                    <span class=\"title no-wrap\">Помощь</span>
                </a>
            </li>
        </ul>
        <div class=\"menubar-foot-panel\">
            <small class=\"no-linebreak hidden-folded\">
                <span class=\"opacity-75\" style=\"border-top: 1px solid #58226e;\">Copyright &copy; 2015 <a href=\"\">www.clapter.ru</a></span>
            </small>
        </div>
    </div>
</div>


";
        // line 65
        echo "    ";
        // line 66
        echo "        ";
        // line 67
        echo "            ";
        // line 68
        echo "            ";
        // line 69
        echo "                ";
        // line 70
        echo "                ";
        // line 71
        echo "                ";
        // line 72
        echo "            ";
        // line 73
        echo "        ";
        // line 74
        echo "        ";
        // line 75
        echo "            ";
        // line 76
        echo "            ";
        // line 77
        echo "                ";
        // line 78
        echo "                ";
        // line 79
        echo "            ";
        // line 80
        echo "        ";
        // line 81
        echo "        ";
        // line 82
        echo "            ";
        // line 83
        echo "            ";
        // line 84
        echo "                ";
        // line 85
        echo "                ";
        // line 86
        echo "                ";
        // line 87
        echo "                ";
        // line 88
        echo "            ";
        // line 89
        echo "        ";
        // line 90
        echo "    ";
    }

    public function getTemplateName()
    {
        return "layout/leftmenu-buyer.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  134 => 90,  132 => 89,  130 => 88,  128 => 87,  126 => 86,  124 => 85,  122 => 84,  120 => 83,  118 => 82,  116 => 81,  114 => 80,  112 => 79,  110 => 78,  108 => 77,  106 => 76,  104 => 75,  102 => 74,  100 => 73,  98 => 72,  96 => 71,  94 => 70,  92 => 69,  90 => 68,  88 => 67,  86 => 66,  84 => 65,  19 => 1,);
    }
}
