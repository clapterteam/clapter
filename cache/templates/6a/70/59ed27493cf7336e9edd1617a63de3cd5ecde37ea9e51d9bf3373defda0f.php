<?php

/* base-layout.html.twig */
class __TwigTemplate_6a7059ed27493cf7336e9edd1617a63de3cd5ecde37ea9e51d9bf3373defda0f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'extrahead' => array($this, 'block_extrahead'),
            'title' => array($this, 'block_title'),
            'keywords' => array($this, 'block_keywords'),
            'robots' => array($this, 'block_robots'),
            'extrastyles' => array($this, 'block_extrastyles'),
            'extrascripts' => array($this, 'block_extrascripts'),
            'header' => array($this, 'block_header'),
            'content' => array($this, 'block_content'),
            'footer' => array($this, 'block_footer'),
            'lastblock' => array($this, 'block_lastblock'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<!--[if lt IE 7 ]><html class=\"ie ie6\"> <![endif]-->
<!--[if IE 7 ]><html class=\"ie ie7\"> <![endif]-->
<!--[if IE 8 ]><html class=\"ie ie8\"> <![endif]-->
<!--[if gte IE 9]><html class=\"ie9\"> <![endif]-->
<!--[if !(IE)]><!--><html> <!--<![endif]-->
<head>
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">
";
        // line 9
        $this->displayBlock('extrahead', $context, $blocks);
        // line 10
        echo "<title>";
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
";
        // line 11
        $this->displayBlock('keywords', $context, $blocks);
        // line 15
        echo "<meta name=\"language\" content=\"ru\">
";
        // line 16
        $this->displayBlock('robots', $context, $blocks);
        // line 19
        $this->displayBlock('extrastyles', $context, $blocks);
        // line 20
        $this->displayBlock('extrascripts', $context, $blocks);
        // line 21
        echo "</head>

<body class=\"";
        // line 23
        echo twig_escape_filter($this->env, ((array_key_exists("body_class", $context)) ? (_twig_default_filter((isset($context["body_class"]) ? $context["body_class"] : null), "")) : ("")), "html", null, true);
        echo "\">
    <div class=\"body\">
";
        // line 25
        $this->displayBlock('header', $context, $blocks);
        // line 26
        $this->displayBlock('content', $context, $blocks);
        // line 27
        $this->displayBlock('footer', $context, $blocks);
        // line 28
        echo "</div>
";
        // line 29
        $this->displayBlock('lastblock', $context, $blocks);
        // line 30
        echo "</body>
</html>";
    }

    // line 9
    public function block_extrahead($context, array $blocks = array())
    {
    }

    // line 10
    public function block_title($context, array $blocks = array())
    {
    }

    // line 11
    public function block_keywords($context, array $blocks = array())
    {
        // line 12
        echo "<meta name=\"description\" content=\"CHANGE ME\">
<meta name=\"keywords\" content=\"CHANGE ME\">
";
    }

    // line 16
    public function block_robots($context, array $blocks = array())
    {
        // line 17
        echo "<meta name=\"robots\" content=\"index, follow\">
";
    }

    // line 19
    public function block_extrastyles($context, array $blocks = array())
    {
    }

    // line 20
    public function block_extrascripts($context, array $blocks = array())
    {
    }

    // line 25
    public function block_header($context, array $blocks = array())
    {
    }

    // line 26
    public function block_content($context, array $blocks = array())
    {
    }

    // line 27
    public function block_footer($context, array $blocks = array())
    {
    }

    // line 29
    public function block_lastblock($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "base-layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  134 => 29,  129 => 27,  124 => 26,  119 => 25,  114 => 20,  109 => 19,  104 => 17,  101 => 16,  95 => 12,  92 => 11,  87 => 10,  82 => 9,  77 => 30,  75 => 29,  72 => 28,  70 => 27,  68 => 26,  66 => 25,  61 => 23,  57 => 21,  55 => 20,  53 => 19,  51 => 16,  48 => 15,  46 => 11,  41 => 10,  39 => 9,  29 => 1,);
    }
}
