<?php

/* searchresult.html.twig */
class __TwigTemplate_70f97ac77c7f49bffa8be2bcf0907136c1f3fb35d5e371b75dc49dfaec084f70 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("layout/layout.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'extrascripts' => array($this, 'block_extrascripts'),
            'content' => array($this, 'block_content'),
            'lastblock' => array($this, 'block_lastblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_extrascripts($context, array $blocks = array())
    {
        // line 4
        echo "    ";
        $this->displayParentBlock("extrascripts", $context, $blocks);
        echo "
    <script type=\"text/javascript\" src=\"js/jquery.ui-slider.js\"></script>
";
    }

    // line 8
    public function block_content($context, array $blocks = array())
    {
        // line 9
        echo "<div class=\"filters\">
        <form action=\"#\" method=\"post\">
            <div class=\"formCost\">
                <label for=\"minCost\">Цена: от</label> <input type=\"text\" id=\"minCost\" value=\"0\">
                <label for=\"maxCost\">до</label> <input type=\"text\" id=\"maxCost\" value=\"1000\">
            </div>
            <div class=\"sliderCont\">
                <div id=\"slider\" class=\"ui-slider ui-slider-horizontal ui-widget ui-widget-content ui-corner-all\">
                    <div class=\"ui-slider-range ui-widget-header\" style=\"left: 0%; width: 100%;\"></div>
                    <a class=\"ui-slider-handle ui-state-default ui-corner-all\" href=\"#\" style=\"left: 0%;\">
                    </a><a class=\"ui-slider-handle ui-state-default ui-corner-all\" href=\"#\" style=\"left: 100%;\"></a></div>
            </div>
        </form>
    </div>

    <select name=\"region\" onchange=\"loadCity(this)\">
        <option>
            ";
        // line 26
        echo (isset($context["state"]) ? $context["state"] : null);
        echo "
        </option>

    </select>

    <select name=\"city\">
        <option>Выберите область</option>
    </select>

<div class=\"container\">
    <div class=\"row\">
        <div class=\"col-md-12\">
            <h3>Результаты поиска</h3>

            <div class=\"tabbable-panel\">
                <div class=\"tabbable-line\">
                    <ul class=\"nav nav-tabs \">
                        <li class=\"active\">
                            <a href=\"#tab_default_1\" data-toggle=\"tab\">
                                Tab 1 </a>
                        </li>
                        <li>
                            <a href=\"#tab_default_2\" data-toggle=\"tab\">
                                Tab 2 </a>
                        </li>
                        <li>
                            <a href=\"#tab_default_3\" data-toggle=\"tab\">
                                Tab 3 </a>
                        </li>
                    </ul>
                    <div class=\"tab-content\">
                        <div class=\"tab-pane active\" id=\"tab_default_1\">
                            ";
        // line 58
        echo (isset($context["result_search1"]) ? $context["result_search1"] : null);
        echo "
                        </div>
                        <div class=\"tab-pane\" id=\"tab_default_2\">
   ";
        // line 61
        echo (isset($context["result_search1"]) ? $context["result_search1"] : null);
        echo "
                        </div>
                        <div class=\"tab-pane\" id=\"tab_default_3\">
                            ";
        // line 64
        echo (isset($context["result_search2"]) ? $context["result_search2"] : null);
        echo "
                        </div>
                    </div>
                </div>
            </div>
";
    }

    // line 70
    public function block_lastblock($context, array $blocks = array())
    {
        // line 71
        echo "    ";
        $this->displayParentBlock("lastblock", $context, $blocks);
        echo "
    <script>
        function loadCity(select)
        {
            var citySelect = \$('select[name=\"city\"]');

            // послыаем AJAX запрос, который вернёт список городов для выбранной области
            \$.getJSON('/ajax/get_city.php', {action:'getCity', region:select.value}, function(cityList){
                citySelect.html(''); // очищаем список городов

                // заполняем список городов новыми пришедшими данными
                console.log(cityList);
                \$.each(cityList, function(i){
                    citySelect.append('<option value=\"' + i + '\">' + this + '</option>');
                });
            });
        }

        \$(document).ready(function() {
            jQuery(\"#slider\").slider({
                min: 0,
                max: 1000,
                values: [0,1000],
                range: true,
                stop: function(event, ui) {
                    jQuery(\"input#minCost\").val(jQuery(\"#slider\").slider(\"values\",0));
                    jQuery(\"input#maxCost\").val(jQuery(\"#slider\").slider(\"values\",1));
                },
                slide: function(event, ui){
                    jQuery(\"input#minCost\").val(jQuery(\"#slider\").slider(\"values\",0));
                    jQuery(\"input#maxCost\").val(jQuery(\"#slider\").slider(\"values\",1));
                }
            });


            jQuery(\"input#minCost\").change(function(){
                var value1=jQuery(\"input#minCost\").val();
                var value2=jQuery(\"input#maxCost\").val();

                if(parseInt(value1) > parseInt(value2)){
                    value1 = value2;
                    jQuery(\"input#minCost\").val(value1);
                }
                jQuery(\"#slider\").slider(\"values\",0,value1);
            });


            jQuery(\"input#maxCost\").change(function(){
                var value1=jQuery(\"input#minCost\").val();
                var value2=jQuery(\"input#maxCost\").val();

                if (value2 > 1000) { value2 = 1000; jQuery(\"input#maxCost\").val(1000)}

                if(parseInt(value1) > parseInt(value2)){
                    value2 = value1;
                    jQuery(\"input#maxCost\").val(value2);
                }
                jQuery(\"#slider\").slider(\"values\",1,value2);
            });
        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "searchresult.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  131 => 71,  128 => 70,  118 => 64,  112 => 61,  106 => 58,  71 => 26,  52 => 9,  49 => 8,  41 => 4,  38 => 3,  11 => 1,);
    }
}
