<?php

/* layout/footer.html.twig */
class __TwigTemplate_23e97b816998f0c8a04dd700f27034cf9bb80e920c8c9cdb54bf073951dd3f5d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"footercontainer\">
    <div id=\"footer\" style=\"text-align: center;\">
        <div class=\"col-md-8 col-md-offset-2 col-lg-8 col-lg-offset-2\">
            <span class=\"opacity-75\" style=\"border-top: 1px solid #58226e;\">Copyright &copy; 2015 <a href=\"\">www.clapter.ru</a></span>
        </div>
    </div>
</div>

";
    }

    public function getTemplateName()
    {
        return "layout/footer.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
