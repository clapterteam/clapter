<?php

/* layout/topmenu-admin.html.twig */
class __TwigTemplate_7eb7a8afb02ab124643e7c65c28a61520ab777a9e106289f021ae64fdf1b9093 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<ul class=\"header-nav top-menu-lk-client clearfix\">
    <li><a href=\"#\">Тикет запросы</a></li>
    <li><a href=\"#\">Транзакции</a></li>
    <li><a href=\"#\">Обратная связь</a></li>
</ul>
";
    }

    public function getTemplateName()
    {
        return "layout/topmenu-admin.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
