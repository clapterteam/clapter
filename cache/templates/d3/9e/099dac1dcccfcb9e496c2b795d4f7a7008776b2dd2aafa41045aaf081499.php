<?php

/* embed/regmodals.html.twig */
class __TwigTemplate_d39e099dac1dcccfcb9e496c2b795d4f7a7008776b2dd2aafa41045aaf081499 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!-- Login modal -->
<div class=\"modal fade\" id=\"login\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"formModalLabel\" aria-hidden=\"true\">
    <div class=\"modal-dialog\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-hidden=\"true\">&times;</button>
                <h4 class=\"modal-title\" id=\"formModalLabel\">Вход в личный кабинет</h4>
            </div>
            <form class=\"form-horizontal ajaxform\" role=\"form\" method=\"POST\">
                <div class=\"modal-body\">
                    <div class=\"message-box\"></div>
                    <div class=\"form-group\">
                        <div class=\"col-sm-3\">
                            <label for=\"username\" class=\"control-label\">Электронная почта: </label>
                        </div>
                        <div class=\"col-sm-9\">
                            <input type=\"text\" name=\"username\" id=\"username\" class=\"form-control\" placeholder=\"Email or Username\">
                        </div>
                    </div>
                    <div class=\"form-group\">
                        <div class=\"col-sm-3\">
                            <label for=\"password\" class=\"control-label\">Пароль:</label>
                        </div>
                        <div class=\"col-sm-9\">
                            <input type=\"password\" name=\"password\" id=\"password\" class=\"form-control\" placeholder=\"Password\">
                        </div>
                    </div>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-default\" data-dismiss=\"modal\">Cancel</button>
                    ";
        // line 32
        echo "                    <input class=\"button expand btn btn-primary\" type=\"submit\" name=\"login\" value=\"Войти\">
                </div>
                <input type=\"hidden\" name=\"action\" value=\"login\">
            </form>
        </div>
    </div>
</div>
<div class=\"modal fade\" id=\"loginOld\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
    <div class=\"modal-dialog\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                <h4 class=\"modal-title\" id=\"myModalLabel\">Log in</h4>
            </div>
            <div class=\"modal-body\">
                <div class=\"medium-12 columns\">
                    <form class=\"ajaxform\" method=\"POST\" action=\"\">
                        <div class=\"message-box\"></div>
                        <input type=\"text\" name=\"username\" placeholder=\"EMAIL OR USERNAME\" >
                        <input type=\"password\" name=\"password\" placeholder=\"PASSWORD\">
                        <input id=\"rememberme\" type=\"checkbox\" name=\"rememberme\">
                        <label class=\"remember\" for=\"rememberme\">Remember me</label>
                        <input type=\"hidden\" name=\"action\" value=\"login\">
                        <input class=\"button expand\" type=\"submit\" name=\"login\" value=\"LOG IN\">
                    </form>
                    <img class=\"or image\" src=\"/lib/login/assets/img/or.png\">
                </div>
                <div class=\"medium-4 columns\">
                    <a onclick='PLS.Alert(\$(\".message-box\"), 2,\"Loading ...\");' href=\"/lib/login/auth/connect.php?method=facebook\" class=\"button expand buttonFacebook\">FACEBOOK</a>
                </div>
                <div class=\"medium-4 columns\">
                    <a onclick='PLS.Alert(\$(\".message-box\"), 2,\"Loading ...\");' href=\"/lib/login/auth/connect.php?method=twitter\" class=\"button expand buttonTwitter\">TWITTER</a>
                </div>
                <div class=\"medium-4 columns\">
                    <a onclick='PLS.Alert(\$(\".message-box\"), 2,\"Loading ...\");' href=\"/lib/login/auth/connect.php?method=google\" class=\"button expand buttonGoogle\">GOOGLE +</a>
                </div>
                <div class=\"medium-12 columns links\">
                    <a class=\"link\" data-toggle=\"modal\" data-target=\"#signup\">Sign Up</a><span class=\"orspan\">-</span>
                    <a class=\"link\" data-toggle=\"modal\" data-target=\"#recover\">Forgot Password</a><span class=\"orspan\">-</span>
                    <a class=\"link\" data-toggle=\"modal\" data-target=\"#reactivate\">Resend Activation Email</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Signup modal -->
<div class=\"modal fade\" id=\"signup\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
    <div class=\"modal-dialog\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                <h4 class=\"modal-title\" id=\"myModalLabel\">Sign up</h4>
            </div>
            <div class=\"modal-body\">
                <div class=\"medium-12 columns\">
                    <form class=\"ajaxform\" method=\"POST\" action=\"\">
                        <div class=\"message-box\"></div>
                        ";
        // line 90
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["USER_ROLES"]) ? $context["USER_ROLES"] : null));
        foreach ($context['_seq'] as $context["roleid"] => $context["rolename"]) {
            // line 91
            echo "                            <input type=\"radio\" name=\"user_role\" value=\"";
            echo twig_escape_filter($this->env, $context["roleid"], "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $context["rolename"], "html", null, true);
            echo "
                        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['roleid'], $context['rolename'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 93
        echo "                        <input type=\"text\" name=\"username\" placeholder=\"USERNAME\">
                        <input type=\"text\" name=\"email\" placeholder=\"EMAIL\">
                        <input type=\"password\" name=\"password\" placeholder=\"PASSWORD\" />
                        <input type=\"password\" name=\"cpassword\" placeholder=\"CONFIRM PASSWORD\" />
                        <div class=\"PLS_recaptcha\"></div>
                        <input type=\"hidden\" name=\"action\" value=\"signup\">
                        <input class=\"button expand\" type=\"submit\" name=\"signup\" value=\"SIGN UP\">
                    </form>
                    <img class=\"or image\" src=\"/lib/login/assets/img/or.png\">
                </div>
                <div class=\"medium-4 columns\">
                    <a onclick='PLS.Alert(\$(\".message-box\"), 2,\"Loading ...\");' href=\"/lib/login/auth/connect.php?method=facebook\" class=\"button expand buttonFacebook\">FACEBOOK</a>
                </div>
                <div class=\"medium-4 columns\">
                    <a onclick='PLS.Alert(\$(\".message-box\"), 2,\"Loading ...\");' href=\"/lib/login/auth/connect.php?method=twitter\" class=\"button expand buttonTwitter\">TWITTER</a>
                </div>
                <div class=\"medium-4 columns\">
                    <a onclick='PLS.Alert(\$(\".message-box\"), 2,\"Loading ...\");' href=\"/lib/login/auth/connect.php?method=google\" class=\"button expand buttonGoogle\">GOOGLE +</a>
                </div>
                <div class=\"medium-12 columns links\">
                    <a class=\"link\" data-toggle=\"modal\" data-target=\"#login\">Log In</a><span class=\"orspan\">-</span>
                    <a class=\"link\" data-toggle=\"modal\" data-target=\"#recover\">Forgot Password</a><span class=\"orspan\">-</span>
                    <a class=\"link\" data-toggle=\"modal\" data-target=\"#reactivate\">Resend Activation Email</a>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Forgot password modal -->
<div class=\"modal fade\" id=\"recover\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
    <div class=\"modal-dialog\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                <h4 class=\"modal-title\" id=\"myModalLabel\">Forgot password</h4>
            </div>
            <div class=\"medium-12 columns\">
                <form class=\"ajaxform\"  method=\"POST\" action=\"\">
                    <div class=\"message-box\"></div>
                    <input type=\"text\" name=\"email\" placeholder=\"EMAIL\"/>
                    <div class=\"PLS_recaptcha\"></div>
                    <input type=\"hidden\" name=\"action\" value=\"recover\">
                    <input class=\"button expand\" type=\"submit\" name=\"recover\" value=\"RESET PASSWORD\">
                </form>
            </div>
            <div class=\"medium-12 columns links\">
                <a class=\"link\" data-toggle=\"modal\" data-target=\"#login\">Log In</a><span class=\"orspan\">-</span>
                <a class=\"link\" data-toggle=\"modal\" data-target=\"#signup\">Sign Up</a><span class=\"orspan\">-</span>
                <a class=\"link\" data-toggle=\"modal\" data-target=\"#reactivate\">Resend Activation Email</a>
            </div>
        </div>
    </div>
</div>

<!-- Send activation email modal -->
<div class=\"modal fade\" id=\"reactivate\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
    <div class=\"modal-dialog\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                <h4 class=\"modal-title\" id=\"myModalLabel\">Send activation email</h4>
            </div>
            <div class=\"modal-body\">
                <div class=\"medium-12 columns\">
                    <form class=\"ajaxform\" method=\"POST\" action=\"\">
                        <div class=\"message-box\"></div>
                        <input type=\"text\" name=\"email\" placeholder=\"EMAIL\"/>
                        <div class=\"PLS_recaptcha\"></div>
                        <input type=\"hidden\" name=\"action\" value=\"reactivate\">
                        <input class=\"button expand\" type=\"submit\" name=\"reactivate\" value=\"SEND EMAIL\">
                    </form>
                </div>
                <div class=\"medium-12 columns links\">
                    <a class=\"link\" data-toggle=\"modal\" data-target=\"#login\">Log In</a><span class=\"orspan\">-</span>
                    <a class=\"link\" data-toggle=\"modal\" data-target=\"#signup\">Sign Up</a><span class=\"orspan\">-</span>
                    <a class=\"link\" data-toggle=\"modal\" data-target=\"#recover\">Forgot Password</a><span class=\"orspan\">-</span>
                </div>
            </div>
        </div>
    </div>
</div>
";
    }

    public function getTemplateName()
    {
        return "embed/regmodals.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  126 => 93,  115 => 91,  111 => 90,  51 => 32,  19 => 1,);
    }
}
