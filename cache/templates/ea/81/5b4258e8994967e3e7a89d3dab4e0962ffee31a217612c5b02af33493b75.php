<?php

/* layout/leftmenu-common.html.twig */
class __TwigTemplate_ea815b4258e8994967e3e7a89d3dab4e0962ffee31a217612c5b02af33493b75 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"menubar\" class=\"menubar-inverse home-bg\">
    <div class=\"menubar-fixed-panel\">
        <div>
            <a class=\"btn btn-icon-toggle btn-default menubar-toggle\" data-toggle=\"menubar\"
               href=\"javascript:void(0);\">
                <i class=\"fa fa-bars\"></i>
            </a>
        </div>
    </div>
    <div class=\"menubar-scroll-panel side-menu\">
        <ul id=\"main-menu\" class=\"gui-controls\">
            <li>
                <a href=\"/index.php\" title=\"\">
                    <div class=\"gui-icon\"><i class=\"fa fa-folder-open fa-fw\"></i></div>
                    <span class=\"title no-wrap\">Поиск запчастей</span>
                </a>
            </li>
            <li>
                <a href=\"/catalog.php\" title=\"\">
                    <div class=\"gui-icon\"><i class=\"fa fa-folder-open fa-fw\"></i></div>
                    <span class=\"title no-wrap\">Каталоги</span>
                </a>
            </li>
            <li>
                <a href=\"/request_vin.php\" title=\"\">
                    <div class=\"gui-icon\"><i class=\"fa fa-folder-open fa-fw\"></i></div>
                    <span class=\"title no-wrap\">Биржа запросов</span>
                </a>
            </li>
            <li>
                <a href=\"/forms.php\" title=\"\">
                    <div class=\"gui-icon\"><i class=\"fa fa-folder-open fa-fw\"></i></div>
                    <span class=\"title no-wrap\">VIN Декодер</span>
                </a>
            </li>
            <li>
                <a href=\"/request_vin.php\" title=\"\">
                    <div class=\"gui-icon\"><i class=\"fa fa-folder-open fa-fw\"></i></div>
                    <span class=\"title no-wrap\">Новостной портал</span>
                </a>
            </li>
            <li>
                <a href=\"/help.php\" title=\"\">
                    <div class=\"gui-icon\"><i class=\"fa fa-folder-open fa-fw\"></i></div>
                    <span class=\"title no-wrap\">Форум</span>
                </a>
            </li>
            <li>
                <a href=\"/feedback.php\" title=\"\">
                    <div class=\"gui-icon\"><i class=\"fa fa-folder-open fa-fw\"></i></div>
                    <span class=\"title no-wrap\">Помощь</span>
                </a>
            </li>

        </ul>
        <div class=\"menubar-foot-panel\">
            <small class=\"no-linebreak hidden-folded\">
                <span class=\"opacity-75\" style=\"border-top: 1px solid #58226e;\">Copyright &copy; 2015 <a href=\"\">www.clapter.ru</a></span>
            </small>
        </div>
    </div>
</div>
";
        // line 64
        echo "    ";
        // line 65
        echo "        ";
        // line 66
        echo "        ";
        // line 67
        echo "        ";
        // line 68
        echo "        ";
        // line 69
        echo "        ";
        // line 70
        echo "        ";
        // line 71
        echo "        ";
        // line 72
        echo "        ";
        // line 73
        echo "        ";
        // line 74
        echo "        ";
        // line 75
        echo "        ";
        // line 76
        echo "    ";
    }

    public function getTemplateName()
    {
        return "layout/leftmenu-common.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  107 => 76,  105 => 75,  103 => 74,  101 => 73,  99 => 72,  97 => 71,  95 => 70,  93 => 69,  91 => 68,  89 => 67,  87 => 66,  85 => 65,  83 => 64,  19 => 1,);
    }
}
