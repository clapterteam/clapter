<?php

/* layout.html.twig */
class __TwigTemplate_a2e7ce0bb1e23dadee3ee5d235181d36f10d4c7a8872c2b360fea73c7d3421ff extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("base-layout.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'extrahead' => array($this, 'block_extrahead'),
            'extrastyles' => array($this, 'block_extrastyles'),
            'extrascripts' => array($this, 'block_extrascripts'),
            'title' => array($this, 'block_title'),
            'header' => array($this, 'block_header'),
            'footer' => array($this, 'block_footer'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base-layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_extrahead($context, array $blocks = array())
    {
        // line 4
        echo "<link rel=\"shortcut icon\" href=\"/images/favicon.ico\">
<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
";
    }

    // line 8
    public function block_extrastyles($context, array $blocks = array())
    {
    }

    // line 11
    public function block_extrascripts($context, array $blocks = array())
    {
        // line 12
        echo "    <link href=\"/css/bootstrap.min.css\" rel=\"stylesheet\">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src=\"//oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js\"></script>
    <script src=\"//oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js\"></script>
    <![endif]-->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src=\"//ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js\"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src=\"/js/bootstrap.min.js\"></script>
";
    }

    // line 25
    public function block_title($context, array $blocks = array())
    {
        echo "CHANGE ME";
    }

    // line 27
    public function block_header($context, array $blocks = array())
    {
        // line 28
        echo "    ";
        $this->env->loadTemplate("header.html.twig")->display($context);
    }

    // line 31
    public function block_footer($context, array $blocks = array())
    {
        // line 32
        echo "    ";
        $this->env->loadTemplate("footer.html.twig")->display($context);
    }

    public function getTemplateName()
    {
        return "layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 32,  87 => 31,  82 => 28,  79 => 27,  73 => 25,  58 => 12,  55 => 11,  50 => 8,  44 => 4,  41 => 3,  11 => 1,);
    }
}
