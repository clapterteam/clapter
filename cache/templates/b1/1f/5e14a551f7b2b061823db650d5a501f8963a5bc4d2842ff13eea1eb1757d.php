<?php

/* layout/leftmenu-admin.html.twig */
class __TwigTemplate_b11f5e14a551f7b2b061823db650d5a501f8963a5bc4d2842ff13eea1eb1757d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div id=\"menubar\" class=\"menubar-inverse home-bg\">
    <div class=\"menubar-fixed-panel\">
        <div>
            <a class=\"btn btn-icon-toggle btn-default menubar-toggle\" data-toggle=\"menubar\"
               href=\"javascript:void(0);\">
                <i class=\"fa fa-bars\"></i>
            </a>
        </div>
    </div>
    <div class=\"menubar-scroll-panel side-menu\">
        <ul id=\"main-menu\" class=\"gui-controls\">
            <li>
                <a href=\"/workspace/index.php\" title=\"\">
                    <div class=\"gui-icon\"><i class=\"fa fa-folder-open fa-fw\"></i></div>
                    <span class=\"title no-wrap\">Рабочий стол</span>
                </a>
            </li>
        </ul>
        <div class=\"menubar-foot-panel\">
            <small class=\"no-linebreak hidden-folded\">
                <span class=\"opacity-75\" style=\"border-top: 1px solid #58226e;\">Copyright &copy; 2015 <a href=\"\">www.clapter.ru</a></span>
            </small>
        </div>
    </div>
</div>

<div class=\"sidebar-nav\">
    <ul class=\"dropdown-menu\" role=\"menu\" aria-labelledby=\"dropdownMenu\" style=\"margin-bottom: 5px; display: block; position: static;\">
        <li><a href=\"/workspace/index.php\">Рабочий стол</a></li>
        <li><a href=\"#\">Страницы</a></li>
        <li><a href=\"#\">Категории</a></li>
        <li class=\"dropdown-submenu\">
            <a tabindex=\"-1\" href=\"#\">Объявления</a>
            <ul class=\"dropdown-menu\">
                <li><a tabindex=\"-1\" href=\"#\">Все позиции загруженных прайс-листов</a></li>
                <li><a href=\"#\">Все объявления</a></li>
                <li><a href=\"#\">Опубликованные позиции</a></li>
                <li><a href=\"#\">Опубликованные объявления</a></li>
                <li><a href=\"#\">Автоматическая проверка позиций</a></li>
                <li><a href=\"#\">Ошибочные позиции</a></li>
                <li><a href=\"#\">Ошибочные объявления</a></li>
                <li><a href=\"#\">Позиции на модерации</a></li>
                <li><a href=\"#\">Объявления на модерации</a></li>
                <li><a href=\"#\">Заблокированные объявления</a></li>
                <li><a href=\"#\">Заблокированные позиции</a></li>
            </ul>
        </li>
        <li class=\"dropdown-submenu\">
            <a tabindex=\"-1\" href=\"#\">Запросы</a>
            <ul class=\"dropdown-menu\">
                <li><a tabindex=\"-1\" href=\"#\">Все запросы</a></li>
                <li><a href=\"#\">Опубликованные запросы</a></li>
                <li><a href=\"#\">Закрытые запросы</a></li>
                <li><a href=\"#\">Выполненные запросы</a></li>
                <li><a href=\"#\">Жалобы по запросам</a></li>
                <li><a href=\"#\">Запросы без ответа продавцов</a></li>
                <li><a href=\"#\">Запросы ожидающий действий пользователей</a></li>
            </ul>
        </li>
        <li class=\"dropdown-submenu\">
            <a tabindex=\"-1\" href=\"#\">Бренды</a>
            <ul class=\"dropdown-menu\">
                <li><a tabindex=\"-1\" href=\"#\">Все</a></li>
                <li><a href=\"#\">Связанные бренды с объявлениями</a></li>
                <li><a href=\"#\">Связанные бренды с каталогами</a></li>
            </ul>
        </li>
        <li class=\"dropdown-submenu\">
            <a tabindex=\"-1\" href=\"#\">Сотрудники</a>
            <ul class=\"dropdown-menu\">
                <li><a tabindex=\"-1\" href=\"#\">СЕО Модератор</a></li>
                <li><a href=\"#\">Модератор</a></li>
                <li><a href=\"#\">Digital маркетинг</a></li>
            </ul>
        </li>
        <li><a href=\"#\">Пользователи</a></li>
        <li><a href=\"#\">Premium пользователи</a></li>
        <li class=\"dropdown-submenu\">
            <a tabindex=\"-1\" href=\"#\">Капитал</a>
            <ul class=\"dropdown-menu\">
                <li><a tabindex=\"-1\" href=\"#\">Баланс</a></li>
                <li><a href=\"#\">Снятые с публикации рекламные компании</a></li>
                <li><a href=\"#\">Оплаченные аккаунты компании</a></li>
                <li><a href=\"#\">Оплаченные рекламные компании</a></li>
                <li><a href=\"#\">Не оплаченные аккаунты компании</a></li>
                <li><a href=\"#\">Транзакции через PayAnyWey</a></li>
                <li><a href=\"#\">Транзакции через расчетный счет</a></li>
                <li><a href=\"#\">Тарифы</a></li>
                <li><a href=\"#\">Расходы на обслуживание</a></li>
            </ul>
        </li>
        <li class=\"dropdown-submenu\">
            <a tabindex=\"-1\" href=\"#\">Каталоги</a>
            <ul class=\"dropdown-menu\">
                <li><a tabindex=\"-1\" href=\"#\">Оригинальные каталоги</a></li>
                <li><a href=\"#\">Неоригинальные каталоги</a></li>
            </ul>
        </li>
        <li><a href=\"#\">VIN Декодер</a></li>
        <li><a href=\"#\">Меню</a></li>
        <li><a href=\"#\">FAQS</a></li>
        <li><a href=\"#\">Блог/Новости/Статьи</a></li>
        <li><a href=\"#\">Файловое хранилище</a></li>
        <li><a href=\"#\">Галерея</a></li>
        <li><a href=\"#\">Комментарии</a></li>
        <li><a href=\"#\">Отзывы</a></li>
        <li><a href=\"#\">Жалобы</a></li>
        <li><a href=\"#\">СМС Оповещение</a></li>
        <li><a href=\"#\">Тикет</a></li>
        <li><a href=\"#\">Обратная связь</a></li>
        <li class=\"dropdown-submenu\">
            <a tabindex=\"-1\" href=\"#\">Статистика</a>
            <ul class=\"dropdown-menu\">
                <li><a tabindex=\"-1\" href=\"#\">Общая посещаемость</a></li>
                <li><a href=\"#\">Посещаемость</a></li>
                <li><a href=\"#\">Зарегистрированные</a></li>
                <li><a href=\"#\">Читателей статей</a></li>
                <li><a href=\"#\">Опубликованные позиции прайс-листа</a></li>
                <li><a href=\"#\">Ошибочные позиции прайс-листа</a></li>
                <li><a href=\"#\">Загружанные прайс-листы</a></li>
                <li><a href=\"#\">Список всех позиций прайс-листов</a></li>
                <li><a href=\"#\">Опубликованные объявления</a></li>
                <li><a href=\"#\">Заблокированные объявления</a></li>
                <li><a href=\"#\">Позиции на модерации</a></li>
                <li><a href=\"#\">Объявления на модерации</a></li>
                <li><a href=\"#\">Платежи</a></li>
                <li><a href=\"#\">Прогноз платежей на следующий месяц</a></li>
                <li><a href=\"#\">Пользователи</a></li>
                <li><a href=\"#\">Компании</a></li>
                <li><a href=\"#\">Автоматическая проверка позиций прайс-листов</a></li>
                <li><a href=\"#\">Декодирование VIN</a></li>
                <li><a href=\"#\">Заблокированные аккаунты</a></li>
                <li><a href=\"#\">Показано телефонов</a></li>
                <li><a href=\"#\">Добавлено в избранное</a></li>
                <li><a href=\"#\">Добавлено в корзину</a></li>
                <li><a href=\"#\">Оформленные заказы</a></li>
            </ul>
        </li>
        <li><a href=\"#\">Города/Области/Регионы</a></li>
        <li><a href=\"#\">Модули</a></li>
        <li><a href=\"#\">Плагины</a></li>
        <li><a href=\"#\">Компоненты</a></li>
        <li><a href=\"#\">Система</a></li>
        <li><a href=\"#\">Реклама</a></li>
        <li class=\"divider\"></li>
        <li><a href=\"/lib/login/auth/logout.php\">Выход</a></li>
    </ul>
</div>";
    }

    public function getTemplateName()
    {
        return "layout/leftmenu-admin.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
