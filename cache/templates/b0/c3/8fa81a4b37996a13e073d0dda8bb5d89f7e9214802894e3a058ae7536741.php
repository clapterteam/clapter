<?php

/* layout/base-layout.html.twig */
class __TwigTemplate_b0c38fa81a4b37996a13e073d0dda8bb5d89f7e9214802894e3a058ae7536741 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'extrahead' => array($this, 'block_extrahead'),
            'title' => array($this, 'block_title'),
            'keywords' => array($this, 'block_keywords'),
            'robots' => array($this, 'block_robots'),
            'extrastyles' => array($this, 'block_extrastyles'),
            'extrascripts' => array($this, 'block_extrascripts'),
            'header' => array($this, 'block_header'),
            'content' => array($this, 'block_content'),
            'leftmenu' => array($this, 'block_leftmenu'),
            'footer' => array($this, 'block_footer'),
            'lastblock' => array($this, 'block_lastblock'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!DOCTYPE html>
<!--[if lt IE 7 ]><html class=\"ie ie6\"> <![endif]-->
<!--[if IE 7 ]><html class=\"ie ie7\"> <![endif]-->
<!--[if IE 8 ]><html class=\"ie ie8\"> <![endif]-->
<!--[if gte IE 9]><html class=\"ie9\"> <![endif]-->
<!--[if !(IE)]><!--><html> <!--<![endif]-->
<head>
<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\">
";
        // line 9
        $this->displayBlock('extrahead', $context, $blocks);
        // line 10
        echo "<title>";
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
";
        // line 11
        $this->displayBlock('keywords', $context, $blocks);
        // line 15
        echo "<meta name=\"language\" content=\"ru\">
";
        // line 16
        $this->displayBlock('robots', $context, $blocks);
        // line 19
        $this->displayBlock('extrastyles', $context, $blocks);
        // line 20
        $this->displayBlock('extrascripts', $context, $blocks);
        // line 21
        echo "</head>

<body class=\"";
        // line 23
        echo twig_escape_filter($this->env, ((array_key_exists("body_class", $context)) ? (_twig_default_filter((isset($context["body_class"]) ? $context["body_class"] : null), "menubar-hoverable header-fixed")) : ("menubar-hoverable header-fixed")), "html", null, true);
        echo "\">
    ";
        // line 24
        $this->displayBlock('header', $context, $blocks);
        // line 25
        echo "    <div id=\"base\">
        <div id=\"content\" class=\"wrapper-2\">
            <div class=\"wrap-table\">
                ";
        // line 28
        $this->displayBlock('content', $context, $blocks);
        // line 29
        echo "            </div>
        </div>
        ";
        // line 31
        $this->displayBlock('leftmenu', $context, $blocks);
        // line 32
        echo "    </div>
    ";
        // line 33
        $this->displayBlock('footer', $context, $blocks);
        // line 34
        echo "    ";
        $this->displayBlock('lastblock', $context, $blocks);
        // line 35
        echo "</body>
</html>";
    }

    // line 9
    public function block_extrahead($context, array $blocks = array())
    {
    }

    // line 10
    public function block_title($context, array $blocks = array())
    {
    }

    // line 11
    public function block_keywords($context, array $blocks = array())
    {
        // line 12
        echo "<meta name=\"description\" content=\"\">
<meta name=\"keywords\" content=\"\">
";
    }

    // line 16
    public function block_robots($context, array $blocks = array())
    {
        // line 17
        echo "<meta name=\"robots\" content=\"index, follow\">
";
    }

    // line 19
    public function block_extrastyles($context, array $blocks = array())
    {
    }

    // line 20
    public function block_extrascripts($context, array $blocks = array())
    {
    }

    // line 24
    public function block_header($context, array $blocks = array())
    {
    }

    // line 28
    public function block_content($context, array $blocks = array())
    {
    }

    // line 31
    public function block_leftmenu($context, array $blocks = array())
    {
    }

    // line 33
    public function block_footer($context, array $blocks = array())
    {
    }

    // line 34
    public function block_lastblock($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "layout/base-layout.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  151 => 34,  146 => 33,  141 => 31,  136 => 28,  131 => 24,  126 => 20,  121 => 19,  116 => 17,  113 => 16,  107 => 12,  104 => 11,  99 => 10,  94 => 9,  89 => 35,  86 => 34,  84 => 33,  81 => 32,  79 => 31,  75 => 29,  73 => 28,  68 => 25,  66 => 24,  62 => 23,  58 => 21,  56 => 20,  54 => 19,  52 => 16,  49 => 15,  47 => 11,  42 => 10,  40 => 9,  30 => 1,);
    }
}
