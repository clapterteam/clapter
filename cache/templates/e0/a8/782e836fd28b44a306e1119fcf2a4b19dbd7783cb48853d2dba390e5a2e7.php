<?php

/* workspace/buyer.html.twig */
class __TwigTemplate_e0a8782e836fd28b44a306e1119fcf2a4b19dbd7783cb48853d2dba390e5a2e7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("layout/layout.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'content' => array($this, 'block_content'),
            'lastblock' => array($this, 'block_lastblock'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<div id=\"content\">
    <section>
        <div class=\"section-body\">
            <div class=\"row\">
                <div class=\"col-md-12\">
                    <div class=\"card\">
                        <div class=\"card-head\">
                            <header>Мои запросы</header>
                        </div>
                        <div class=\"tabbable-panel\">
                            <div class=\"tabbable-line\">
                                <div class=\"btn-group pull-right\" data-position=\"toast-top-right\">
                                    <button id=\"newzaprosbtn\" type=\"button\" class=\"btn btn-success\" data-toggle=\"modal\" data-target=\"#myModal\">Новый запрос</button>
                                </div>
                                <ul class=\"nav nav-tabs\">
                                    <li class=\"active\"><a href=\"#first1\" data-toggle=\"tab\">Запчасти</a></li>
                                    <li class=\"\"><a href=\"#second1\" data-toggle=\"tab\">Работы</a></li>
                                </ul>
                                <div class=\"tab-content\">
                                    <div class=\"tab-pane active\" id=\"first1\">
                                        ";
        // line 24
        echo (isset($context["result_tab1"]) ? $context["result_tab1"] : null);
        echo "
                                    </div>
                                    <div class=\"tab-pane\" id=\"second1\">
                                        ";
        // line 27
        echo (isset($context["result_tab2"]) ? $context["result_tab2"] : null);
        echo "
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>

<!-- Modal -->
<div class=\"modal fade\" id=\"myModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
    <div class=\"modal-dialog\">
        <div class=\"modal-content\">
            <div class=\"modal-header\">
                <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\"><span aria-hidden=\"true\">&times;</span></button>
                <h4 class=\"modal-title\" id=\"myModalLabel\">Новый запрос</h4>
            </div>
            <div class=\"modal-body\">
                ";
        // line 48
        $this->env->loadTemplate("embed/formzapros.html.twig")->display($context);
        // line 49
        echo "            </div>
            <div class=\"modal-footer\">
                <button type=\"button\" class=\"btn btn-default\">Назад</button>
                <button type=\"button\" class=\"btn btn-primary\">Следующий шаг</button>
                <button type=\"button\" class=\"btn btn-default\">Отправить</button>
            </div>
        </div>
    </div>
</div>
";
    }

    // line 60
    public function block_lastblock($context, array $blocks = array())
    {
        // line 61
        echo "    ";
        $this->displayParentBlock("lastblock", $context, $blocks);
        echo "
    <script type=\"text/javascript\">
        function setTabActive(num) {
            \$('#tab'+num).find('a').click();
        }

        \$(document).ready(function() {
            \$('#newzaprosbtn').on('click', function() {
                setTabActive(1);
            })
        });
    </script>
";
    }

    public function getTemplateName()
    {
        return "workspace/buyer.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  110 => 61,  107 => 60,  94 => 49,  92 => 48,  68 => 27,  62 => 24,  40 => 4,  37 => 3,  11 => 1,);
    }
}
