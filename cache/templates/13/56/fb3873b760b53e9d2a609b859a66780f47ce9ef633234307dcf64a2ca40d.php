<?php

/* layout/leftmenu-company.html.twig */
class __TwigTemplate_1356fb3873b760b53e9d2a609b859a66780f47ce9ef633234307dcf64a2ca40d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"sidebar-nav\">
    <div class=\"well\" style=\"padding: 8px 0;\">
        <ul class=\"nav nav-list\">
            <li class=\"nav-header\">Left menu Company</li>
            <li><a href=\"index\"><i class=\"icon-home\"></i> Dashboard</a></li>
            <li><a href=\"#\"><i class=\"icon-envelope\"></i> Messages <span class=\"badge badge-info\">4</span></a></li>
            <li><a href=\"#\"><i class=\"icon-comment\"></i> Comments <span class=\"badge badge-info\">10</span></a></li>
            <li class=\"active\"><a href=\"#\"><i class=\"icon-user\"></i> Members</a></li>
            <li class=\"divider\"></li>
            <li><a href=\"#\"><i class=\"icon-comment\"></i> Settings</a></li>
            <li><a href=\"#\"><i class=\"icon-share\"></i> Logout</a></li>
        </ul>
    </div>
</div>";
    }

    public function getTemplateName()
    {
        return "layout/leftmenu-company.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
