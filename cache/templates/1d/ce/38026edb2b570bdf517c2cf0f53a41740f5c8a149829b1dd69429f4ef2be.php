<?php

/* exception.html.twig */
class __TwigTemplate_1dce38026edb2b570bdf517c2cf0f53a41740f5c8a149829b1dd69429f4ef2be extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        try {
            $this->parent = $this->env->loadTemplate("layout/base-layout.html.twig");
        } catch (Twig_Error_Loader $e) {
            $e->setTemplateFile($this->getTemplateName());
            $e->setTemplateLine(1);

            throw $e;
        }

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "layout/base-layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 3
    public function block_content($context, array $blocks = array())
    {
        // line 4
        echo "<h1>Error happens!</h1>
<p><em>";
        // line 5
        echo twig_escape_filter($this->env, (isset($context["message"]) ? $context["message"] : null), "html", null, true);
        echo "</em></p>
";
        // line 6
        if ((isset($context["backtrace"]) ? $context["backtrace"] : null)) {
            // line 7
            echo "<p>
    File: ";
            // line 8
            echo twig_escape_filter($this->env, (isset($context["file"]) ? $context["file"] : null), "html", null, true);
            echo " Line: ";
            echo twig_escape_filter($this->env, (isset($context["line"]) ? $context["line"] : null), "html", null, true);
            echo "
</p>
<pre>
    ";
            // line 11
            echo twig_escape_filter($this->env, (isset($context["backtrace"]) ? $context["backtrace"] : null), "html", null, true);
            echo "
</pre>
";
        }
        // line 14
        echo "<div style=\"text-align: center\"><a href=\"/\">Home</a></div>
";
    }

    public function getTemplateName()
    {
        return "exception.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  65 => 14,  59 => 11,  51 => 8,  48 => 7,  46 => 6,  42 => 5,  39 => 4,  36 => 3,  11 => 1,);
    }
}
