<?php

/* layout/topmenu-seller.html.twig */
class __TwigTemplate_46db333cfe58e0217c5aa1b612e024953b6c4b3b5381fe6516e26d57127ce170 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<ul class=\"header-nav top-menu-lk-client clearfix\">
    <li><a href=\"#\">Заказы продавцам</a></li>
    <li><a href=\"#\">Заказы</a></li>
    <li><a href=\"#\">Баланс: 0 Р</a></li>
    <li><a href=\"#\">Публикация активна: 89 дней</a></li>
</ul>
";
    }

    public function getTemplateName()
    {
        return "layout/topmenu-seller.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
