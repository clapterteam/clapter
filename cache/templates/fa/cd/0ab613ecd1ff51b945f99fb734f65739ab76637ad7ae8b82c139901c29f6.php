<?php

/* layout/topmenu-buyer.html.twig */
class __TwigTemplate_facd0ab613ecd1ff51b945f99fb734f65739ab76637ad7ae8b82c139901c29f6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<ul class=\"header-nav top-menu-lk-client clearfix\">
    <li><a href=\"#\">Предложение продавцов</a></li>
    <li><a href=\"#\">Мое избранное</a></li>
    <li><a href=\"#\">Корзина</a></li>
</ul>
";
    }

    public function getTemplateName()
    {
        return "layout/topmenu-buyer.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
