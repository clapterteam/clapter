<?php

/* layout/topmenu-common.html.twig */
class __TwigTemplate_348ccb55d575e1dcfd948b7a46b4ef4e73c3cf3128a631d0f518bebb6c4e70d3 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<ul class=\"header-nav top-menu-lk-client clearfix\">
    <li><a href=\"/archive.php\">Владельцам автотранспорта</a></li>
    <li><a href=\"/catalog.php\">Магазинам запчастей</a></li>
    <li><a href=\"/request_vin.php\">Сервисным центрам</a></li>
    <li><a href=\"/help.php\">Авторазборкам</a></li>
</ul>
";
    }

    public function getTemplateName()
    {
        return "layout/topmenu-common.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  19 => 1,);
    }
}
