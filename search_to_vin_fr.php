<?php
require_once 'include/config.inc.php';
class VinSaerchPage extends CLPublicPage {
    public function dispatch() {
        $search_vin='1';
        if (isset($_POST["search_vin"])) {
            echo $_POST["search_vin"];
            $search_vin=$_POST["search_vin"];
        }

        $xcrud_vin = Xcrud::get_instance();
        $xcrud_vin->table('vin_code');
        // $xcrud_vin->join('id_user','user','id');
        $xcrud_vin->where('vin =',$search_vin);
        $xcrud_vin->unset_print();
        $xcrud_vin->unset_csv();
        $xcrud_vin->unset_remove();
        $xcrud_vin->unset_edit();
        $xcrud_vin->unset_add();
        $xcrud_vin->unset_view();
        $xcrud_vin->unset_numbers();
        $xcrud_vin->unset_title();
        $search_vin_result = $xcrud_vin->render();
        $ctx = array('search_vin_result'=>$search_vin_result);
        return $this->render('searchvinresult.html.twig', $ctx);
    }
}

$view = new VinSaerchPage();
$view->run();

