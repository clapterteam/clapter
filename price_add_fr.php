<?php
require_once 'include/config.inc.php';
class PriceADD extends CLPublicPage {
    public function dispatch()
    {

        $ctx = array();
        return $this->render('workspace/price_add.html.twig', $ctx);
    }
}
$view = new PriceADD();
$view->run();